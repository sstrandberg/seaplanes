﻿using UnityEngine;
using UnityEngine.Events;

public class WagonWheelTester : MonoBehaviour
{
    [Range(0, 1.8f)]
    public float RPM_x1000;
    public float RotationPerFrame;

    public int BladeCount = 2;
    public int MaxSpokes = 8;

    //public float Mod1;
    //public float Mod2;
    //public float Mod3;
    //public float Mod4;
    //public float Mod5;
    //public float Mod6;
    //public float Mod7;
    //public float Mod8;

    public float ShownSpokes;

    public Transform PropTransform;
    public Material PropMaterial;
    public Transform ComparisonTransform;

    void OnValidate()
    {
        RotationPerFrame = RPM_x1000 * 1000 / 60f / 60f;

        //var rpf = RotationPerFrame;
        //if (rpf == 0) rpf = 1;

        //ShownSpokes = 1f / (rpf % 1);

        //var smallestError = (0.5f + rpf) % 1 - 0.5f;
        //ShownSpokes = BladeCount;
        //for (int i = 8; i > 0; i--)
        //{
        //    if (i % BladeCount != 0) continue;

        //    var fraction = 1 / (float)i;
        //    var error = (0.5f * fraction + rpf) % fraction - 0.5f * fraction;
        //    if (Mathf.Abs(error) * (float)i < Mathf.Abs(smallestError) + Mathf.Epsilon)
        //    {
        //        smallestError = error * (float)i;
        //        ShownSpokes = i;
        //    }
        //}

        //Mod2 = (0.5f * 1 / 1f + rpf) % 1 / 1f - 0.5f * 1 / 1f;
        //Mod2 = (0.5f * 1 / 2f + rpf) % 1 / 2f - 0.5f * 1 / 2f;
        //Mod3 = (0.5f * 1 / 3f + rpf) % 1 / 3f - 0.5f * 1 / 3f;
        //Mod4 = (0.5f * 1 / 4f + rpf) % 1 / 4f - 0.5f * 1 / 4f;
        //Mod5 = (0.5f * 1 / 5f + rpf) % 1 / 5f - 0.5f * 1 / 5f;
        //Mod6 = (0.5f * 1 / 6f + rpf) % 1 / 6f - 0.5f * 1 / 6f;
        //Mod7 = (0.5f * 1 / 7f + rpf) % 1 / 7f - 0.5f * 1 / 7f;
        //Mod8 = (0.5f * 1 / 8f + rpf) % 1 / 8f - 0.5f * 1 / 8f;
    }

    void Start()
    {
        PropMaterial.SetFloat("_ArcCount", 2);
    }

    void Update()
    {
        RotationPerFrame = RPM_x1000 / 3.6f;

        ComparisonTransform.Rotate(Vector3.forward, 360 * RotationPerFrame, Space.World);

        var rpf = RotationPerFrame;

        var smallestError = (0.5f * BladeCount + rpf) % 1 - 0.5f * BladeCount;
        var spokeCount = 1;
        for (int i = MaxSpokes; i > 0; i--)
        {
            if (i % BladeCount != 0) continue;

            var fraction = 1 / (float)i;
            var error = (0.5f * fraction + rpf) % fraction - 0.5f * fraction;
            if (Mathf.Abs(error) * (float)i < Mathf.Abs(smallestError) + 0.001f)
            {
                smallestError = error * (float)i;
                spokeCount = i;
            }
        }
        
        //if(spokeCount % BladeCount != 0)
        //{
        //    spokeCount *= BladeCount;
        //}

        PropMaterial.SetFloat("_ArcCount", spokeCount);
        PropMaterial.SetFloat("_ArcPositionShift", smallestError);
        //PropTransform.Rotate(Vector3.forward, 360 * smallestError / spokeCount, Space.World);
        PropTransform.Rotate(Vector3.forward, 360 * RotationPerFrame, Space.World);


        ShownSpokes = spokeCount;
    }
}