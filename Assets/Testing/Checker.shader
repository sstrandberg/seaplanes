﻿Shader "Custom/Checkers" {
	Properties{
		_Color1("Color 1", Color) = (1,1,1,1)
		_Color2("Color 2", Color) = (0,0,0,1)
		_Smoothness("Smoothness", float) = 0
		_Scale("Scale", Float) = 1
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0

		sampler2D _MainTex;

	struct Input {
		float3 worldPos;
	};

	fixed4 _Color1;
	fixed4 _Color2;
	float _Scale;
	float _Smoothness;

	void surf(Input IN, inout SurfaceOutputStandard o) {

		fixed4 final;
		float pos = 0;
		pos += floor(fmod(abs(IN.worldPos.x / _Scale) + 0.5001, 2));
		//pos += floor(fmod(abs(IN.worldPos.y / _Scale) + 0.5001, 2));
		pos += floor(fmod(abs(IN.worldPos.z / _Scale) + 0.5001, 2));
		pos = fmod(pos, 2);
		final = lerp(_Color1, _Color2, pos);

		o.Albedo = final.rgb;
		o.Smoothness = _Smoothness;
	}
	ENDCG
	}
		FallBack "Diffuse"
}