﻿using UnityEngine;

public class BulletTestFirer : MonoBehaviour
{
    public Bullet Prefab;
    public float Velocity = 300;


    public bool FireNow;
    public float ROF;
    public bool Repeat;

    private float firecool;

    void Update()
    {
        if(FireNow)
        {
            FireNow = false;
            Fire();
        }

        if(Repeat)
        {
            firecool += Time.deltaTime * ROF;
            if(firecool > 1)
            {
                Fire();
                firecool--;
            }
        }
    }

    void Fire()
    {
        var b = Instantiate<Bullet>(Prefab);
        b.gameObject.SetActive(true);

        b.transform.position = transform.position;
        b.Velocity = transform.forward * Velocity;

    }
}