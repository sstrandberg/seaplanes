﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RefractorTest))]
public class RefractorTestEditor : Editor
{

    void OnSceneGUI()
    {

    }


    void OnSceneGUI3DFind()
    {
        var t = (RefractorTest)target;

        t.EyePoint = Handles.PositionHandle(t.EyePoint, Quaternion.identity);


        var eyeA = t.EyePoint - t.EyeOffset;
        var eyeB = t.EyePoint + t.EyeOffset;

        t.VirtualPosition = Handles.PositionHandle(t.VirtualPosition, Quaternion.identity);
        if (t.VirtualPosition.y > 0) t.VirtualPosition.y = 0;

        Handles.DrawLine(eyeA, t.VirtualPosition);
        Handles.DrawLine(eyeB, t.VirtualPosition);

        var planeIntersectA = flatIntercept(eyeA, t.VirtualPosition - eyeA);
        var planeIntersectB = flatIntercept(eyeB, t.VirtualPosition - eyeB);

        var normal = t.Normal.normalized;
        if (normal == Vector3.zero) normal = Vector3.up;
        var refractionIndex = 1.3333f;


        Handles.color = Color.blue;
        Handles.DrawLine(planeIntersectA, planeIntersectA + normal * 50);
        Handles.DrawLine(planeIntersectB, planeIntersectB + normal * 50);

        var theta1A = Mathf.Deg2Rad * Vector3.Angle(eyeA - t.VirtualPosition, normal);
        var theta2A = Mathf.Asin(Mathf.Sin(theta1A) / refractionIndex);

        var theta1B = Mathf.Deg2Rad * Vector3.Angle(eyeB - t.VirtualPosition, normal);
        var theta2B = Mathf.Asin(Mathf.Sin(theta1B) / refractionIndex);

        var crossA = Vector3.Cross(t.VirtualPosition - eyeA, normal);
        var crossB = Vector3.Cross(t.VirtualPosition - eyeB, normal);



        Handles.color = Color.red;
        Handles.DrawLine(planeIntersectA, planeIntersectA + crossA * 10);

        var trueDirA = rotateVector(-normal, theta2A, crossA.normalized);
        var trueDirB = rotateVector(-normal, theta2B, crossB.normalized);

        Handles.color = Color.gray;
        Handles.DrawLine(planeIntersectA, planeIntersectA + trueDirA * 50);
        Handles.DrawLine(planeIntersectB, planeIntersectB + trueDirB * 50);


        var truePosition = lineLineIntersection(planeIntersectA, trueDirA, planeIntersectB, trueDirB);
        Handles.color = Color.Lerp(Color.red, Color.yellow, 0.5f);

        Handles.SphereHandleCap(0, truePosition, Quaternion.identity, 0.3f, EventType.Repaint);
    }

    Vector3 flatIntercept(Vector3 position, Vector3 direction)
    {
        return position + (-position.y / direction.y) * direction;
        //return planeCollision(position, direction, Vector3.zero, Vector3.up);
    }

    Vector3 planeCollision(Vector3 position, Vector3 direction, Vector3 planePoint, Vector3 planeNormal)
    {
        var d = Vector3.Dot((planePoint - position), planeNormal) / Vector3.Dot(direction, planeNormal);
        return d * direction + position;
    }



    Vector3 lineLineIntersection(Vector3 posA, Vector3 dirA, Vector3 posB, Vector3 dirB)
    {
        var sign = Mathf.Sign(Vector3.Dot(Vector3.Cross(dirB, posB - posA), Vector3.Cross(dirB, dirA)));
        return posA + sign * Vector3.Cross(dirB, posB - posA).magnitude / Vector3.Cross(dirB, dirA).magnitude * dirA;
    }

    Vector3 rotateVector(Vector3 v, float angle, Vector3 k)
    {
        return v * Mathf.Cos(angle) + cross(k, v) * Mathf.Sin(angle) + k * Vector3.Dot(k, v) * (1 - Mathf.Sin(angle));
    }

    Vector3 cross(Vector3 a, Vector3 b)
    {
        return new Vector3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
    }
}