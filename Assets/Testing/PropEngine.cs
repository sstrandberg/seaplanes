﻿using UnityEngine;

public class PropEngine : MonoBehaviour
{

    //power -> rpm
    //power increases heat
    //radiator decreases heat (based on heat?)
    //

    //overheat start (engine effectiveness starts being reduced)
    //overheat max (engine effectiveness zero)

    //cooling = dT * airspeed * radiationness

    //power makes rpm go up
    //responsive force + friction makes rpm go down


    //inertia?


    //power = basepower * air * throttle * overheatpenalty
    //heat = power * heatfactor
    //cooling = temperaturedifference * airspeed * radiationness

    public float RPM;
    public float EngineTemperature;
    public float OutputPower;

    [Header("Engine stats")]
    public float BasePower = 100;
    public float FlywheelMass = 1;
    public float FlywheelFriction = 0.001f;
    public float MaxRPM = 2000;

    [Header("Cooling")]
    public float MinOverheat = 100;
    public float MaxOverheat = 110;
    [Range(0, 1)]
    public float OverheatPowerloss = 0.5f;
    public float HeatGeneration = 1;

    public float RadiationPower = 1;

    [Header("Props")]
    public Vector3 PropLocation;
    public float SurfaceArea = 1;
    public float MSPerRPM = 4;


    private PlaneManager pc;
    private Rigidbody rb;

    void Start()
    {
        EngineTemperature = Environment.Instance.AirTemperature;
        pc = GetComponentInParent<PlaneManager>();
        rb = GetComponentInParent<Rigidbody>();
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.TransformPoint(PropLocation), 1);
    }

    void OnValidate()
    {
        Limit.OverEpsilon(ref FlywheelMass);
        Limit.OverEpsilon(ref FlywheelFriction);
        Limit.OverEpsilon(ref MSPerRPM);
    }

    void FixedUpdate()
    {
        var overheatpenalty = Mathx.Map(EngineTemperature, MinOverheat, MaxOverheat, 1, 1 - OverheatPowerloss);
        var rpmPenalty = Mathx.Map(RPM, MaxRPM, MaxRPM + 100, 1, 0);
        var airdensity = Environment.Instance.AirDensity(transform.position.y);
        var power = BasePower * pc.Throttle * airdensity * overheatpenalty * rpmPenalty;
        OutputPower = power;


        RPM += Time.fixedDeltaTime * power / FlywheelMass;
        RPM -= RPM * FlywheelFriction * Time.fixedDeltaTime / FlywheelMass;

        var deltatemperature = Environment.Instance.AirTemperature - EngineTemperature;
        EngineTemperature += HeatGeneration * power * Time.fixedDeltaTime;
        EngineTemperature += Time.fixedDeltaTime * deltatemperature * RadiationPower * (rb.velocity.magnitude + 10) * airdensity;

        var inflow = Vector3.Dot(rb.velocity, transform.forward);
        var outflow = RPM * MSPerRPM;

        //var force = q * 0.5f * SurfaceArea * Mathf.Sign(ds);
        var force = 0.5f * airdensity * SurfaceArea * (outflow * outflow - inflow * inflow);

        rb.AddForceAtPosition(transform.forward * force, transform.TransformPoint(PropLocation));

        RPM -= Time.fixedDeltaTime * force / FlywheelMass;
    }
}