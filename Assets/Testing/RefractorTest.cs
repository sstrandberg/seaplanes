﻿using UnityEngine;
using UnityEditor;

public class RefractorTest : MonoBehaviour
{

    public Vector3 EyePoint;
    public Vector3 EyeOffset;

    public Vector3 Normal;

    public Vector3 VirtualPosition;
}