﻿using UnityEngine;

public class PositionOscilator : MonoBehaviour
{
    public Vector3 Origin;
    public Vector3 Offset;
    public float Frequency = 1;

    private float timer;

    void OnValidate()
    {
        transform.position = Origin;
    }

    void FixedUpdate()
    {
        timer += Time.fixedDeltaTime * Frequency;
        transform.position = Origin + Offset * Mathf.Sin(timer * Mathf.PI * 2);
    }
}