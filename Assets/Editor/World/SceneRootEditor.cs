﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;


[CustomEditor(typeof(SceneRoot))]
[CanEditMultipleObjects]
public class SceneRootEditor : Editor
{

    public override void OnInspectorGUI()
    {
        //var s = SceneManager.GetActiveScene();
        //EditorGUILayout.LabelField(s.name);
        //var s = SceneManager.GetAllScenes();
        var count = SceneManager.sceneCount;
        for (int i = 0; i < count; i++)
        {
            var s = SceneManager.GetSceneAt(i);
            EditorGUILayout.LabelField(s.name);
        }

        var count2 = SceneManager.sceneCountInBuildSettings;

        base.OnInspectorGUI();
    }
}