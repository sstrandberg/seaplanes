﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Map list")]
public class MapList : ScriptableObject
{
    public SceneAsset MasterScene;
    public List<CoordScene> SubScenes;

    public void MoveScene(int oldx, int oldy, int newx, int newy)
    {
        foreach(var cs in SubScenes)
        {
            if(cs.X == newx && cs.Y == newy)
            {
                return;
            }
        }

        for (int i = 0; i < SubScenes.Count; i++)
        {
            var cs = SubScenes[i];

            if(cs.X == oldx && cs.Y == oldy)
            {
                cs.X = newx;
                cs.Y = newy;

                SubScenes[i] = cs;
                break;
            }
        }
    }

    public bool HasScene(SceneAsset s)
    {
        foreach(var cs in SubScenes)
        {
            if(cs.Scene == s)
            {
                return true;
            }
        }
        return false;
    }

    public void RemoveScene(int x, int y)
    {
        for (int i = 0; i < SubScenes.Count; i++)
        {
            var cs = SubScenes[i];
            if (cs.X == x && cs.Y == y)
            {
                SubScenes.RemoveAt(i);
                return;
            }
        }
    }
    public int SceneCount()
    {
        return SubScenes.Count;
    }
}

[Serializable]
public struct CoordScene
{
    public int X;
    public int Y;
    public SceneAsset Scene;
}

#endif