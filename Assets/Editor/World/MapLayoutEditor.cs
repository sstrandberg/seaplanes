﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Collections.Generic;

public class MapLayoutEditor : EditorWindow
{
    public MapList LayoutData;

    private MapManager master;
    private int oldX;
    private int oldY;
    private int coordX;
    private int coordY;

    private int minX;
    private int minY;
    private int maxX;
    private int maxY;

    private int moveX;
    private int moveY;

    private bool areyousure;

    private string buildStatus;

    private SceneAsset selectedScene;

    [MenuItem("Window/Map layout")]
    static void Init()
    {
        GetWindow<MapLayoutEditor>().Show();
    }

    public void OnGUI()
    {
        bool hasFile = LayoutData != null;

        if (!hasFile)
        {
            EditorGUILayout.HelpBox("No map file!", MessageType.Error);
        }
        else
        {
            EditorGUILayout.LabelField(AssetDatabase.GetAssetPath(LayoutData.MasterScene));
            drawTopButtons();
            drawMap();
            drawCoords();
            drawCellThings();
        }
    }

    private void drawTopButtons()
    {
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Reset editor"))
        {
            resetEditor();
        }
        EditorGUILayout.BeginVertical();
        if (GUILayout.Button("Update build and master"))
        {
            buildStatus = updateBuildAndMaster();
        }
        EditorGUILayout.LabelField(buildStatus);
        EditorGUILayout.EndVertical();

        if(GUILayout.Button("Unload scenes"))
        {
            unloadScenes();
        }

        EditorGUILayout.EndHorizontal();
    }

    private void drawCoords()
    {
        oldX = coordX;
        oldY = coordY;

        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField(" ");

        EditorGUILayout.LabelField("Currently at (" + coordX.ToString() + ", " + coordY.ToString() + ").", GUILayout.Width(150));

        if (GUILayout.Button("^", GUILayout.Width(40))) coordY--;
        if (GUILayout.Button("v", GUILayout.Width(40))) coordY++;
        if (GUILayout.Button("<", GUILayout.Width(40))) coordX--;
        if (GUILayout.Button(">", GUILayout.Width(40))) coordX++;
        if (GUILayout.Button("(0,0)", GUILayout.Width(80)))
        {
            coordX = 0;
            coordY = 0;
        }

        EditorGUILayout.EndHorizontal();

        if (oldX != coordX || oldY != coordY)
        {
            onMove();
        }
    }

    private void onMove()
    {
        updateScenePositions();
        selectedScene = null;
        moveX = coordX;
        moveY = coordY;
        areyousure = false;
    }

    private void drawMap()
    {
        int halfWidth = 10;
        int halfHeight = 10;

        int charWidth = 22;

        EditorGUILayout.BeginHorizontal();

        bool firstHorizont = true;
        for (int i = coordX - halfWidth; i < coordX + halfWidth; i++)
        {

            EditorGUILayout.BeginVertical();
            if (firstHorizont)
            {
                EditorGUILayout.LabelField(" ", GUILayout.Width(charWidth));

                for (int j = coordY - halfHeight; j < coordY + halfHeight; j++)
                {
                    EditorGUILayout.LabelField(j.ToString(), GUILayout.Width(charWidth));
                }
            }
            EditorGUILayout.EndVertical();

            bool firstVert = true;

            EditorGUILayout.BeginVertical();
            for (int j = coordY - halfHeight; j < coordY + halfHeight; j++)
            {
                if (firstVert)
                {
                    firstVert = false;
                    EditorGUILayout.LabelField(i.ToString(), GUILayout.Width(charWidth));
                }

                string s = "·";
                
                if(hasScene(i, j))
                {
                    s = "S";
                }

                if (i == coordX && j == coordY)
                {
                    s = "[" + s + "]";
                }
                else
                {
                    s = " " + s + " ";
                }

                EditorGUILayout.LabelField(s, GUILayout.Width(charWidth));
            }
            EditorGUILayout.EndVertical();

            if (firstHorizont)
            {
                firstHorizont = false;
            }
        }

        EditorGUILayout.EndHorizontal();
    }

    private void drawCellThings()
    {
        if (hasScene(coordX, coordY))
        {
            var cs = getScene(coordX, coordY);
            EditorGUILayout.LabelField(AssetDatabase.GetAssetPath(cs.Scene));

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Move: (", GUILayout.Width(50));
            moveX = EditorGUILayout.IntField(moveX, GUILayout.Width(20));
            EditorGUILayout.LabelField(",", GUILayout.Width(10));
            moveY = EditorGUILayout.IntField(moveY, GUILayout.Width(20));
            EditorGUILayout.LabelField(")", GUILayout.Width(10));

            if(hasScene(moveX, moveY))
            {
                EditorGUILayout.LabelField("Occupied!");
            }
            else
            {
                if(GUILayout.Button("Go!"))
                {
                    Undo.RecordObject(LayoutData, "Moving scene");
                    
                    LayoutData.MoveScene(coordX, coordY, moveX, moveY);
                    coordX = moveX;
                    coordY = moveY;

                    EditorUtility.SetDirty(LayoutData);

                    resetEditor();
                }
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            if(!areyousure)
            {
                EditorGUILayout.LabelField("");
                if (GUILayout.Button("Remove?"))
                {
                    areyousure = true;
                }
                
            }
            else
            {
                if (GUILayout.Button("YES"))
                {
                    areyousure = false;
                    Undo.RecordObject(LayoutData, "Removing scene");
                    LayoutData.RemoveScene(coordX, coordY);
                    EditorUtility.SetDirty(LayoutData);
                    resetEditor();
                }
                if (GUILayout.Button("NO"))
                {
                    areyousure = false;
                }
            }
            EditorGUILayout.EndHorizontal();
        }
        else
        {
            EditorGUILayout.LabelField("No scene");
            EditorGUILayout.BeginHorizontal();
            selectedScene = (SceneAsset)EditorGUILayout.ObjectField(selectedScene, typeof(SceneAsset), false);


            if (GUILayout.Button("Add"))
            {
                bool success = tryAddScene();
            }
            if(selectedScene == null)
            {
                EditorGUILayout.LabelField("Select a scene");
            }
            else if(LayoutData.HasScene(selectedScene))
            {
                EditorGUILayout.LabelField("Already exists");
            }
            else
            {
                EditorGUILayout.LabelField("OK");
            }
            EditorGUILayout.EndHorizontal();
        }
    }

    private void checkNullLists()
    {
        if (LayoutData.SubScenes == null)
        {
            LayoutData.SubScenes = new List<CoordScene>();
        }
    }

    private bool hasScene(int x, int y)
    {
        checkNullLists();

        foreach (var s in LayoutData.SubScenes)
        {
            if (s.X == x && s.Y == y)
            {
                return true;
            }
        }
        return false;
    }

    private CoordScene getScene(int x, int y)
    {
        foreach (var s in LayoutData.SubScenes)
        {
            if (s.X == x && s.Y == y)
            {
                return s;
            }
        }

        throw new System.Exception("no scene here mister");
    }

    private void resetEditor()
    {
        EditorSceneManager.OpenScene(AssetDatabase.GetAssetPath(LayoutData.MasterScene));

        refreshScenes();
        updateScenePositions();
    }

    private void refreshScenes()
    {
        foreach (var cs in LayoutData.SubScenes)
        {
            var scene = EditorSceneManager.OpenScene(AssetDatabase.GetAssetPath(cs.Scene), OpenSceneMode.Additive);
            var ro = scene.GetRootGameObjects();
            bool loadError = false;
            if (ro.Length > 0)
            {
                var sr = ro[0].GetComponent<SceneRoot>();
                if (sr == null)
                {
                    loadError = true;
                }
                else
                {
                    Undo.RecordObject(sr, "Updating coordinates");
                    sr.CoordX = cs.X;
                    sr.CoordY = cs.Y;

                    sr.Reposition(coordX, coordY);
                }
            }
            else
            {
                loadError = true;
            }

            if (loadError)
            {
                Debug.LogError(scene.name + " didn't load correctly!");
            }
        }
    }

    private void unloadScenes()
    {
        EditorSceneManager.OpenScene(AssetDatabase.GetAssetPath(LayoutData.MasterScene));
    }

    private void updateScenePositions()
    {
        var roots = FindObjectsOfType<SceneRoot>();
        foreach (var r in roots)
        {
            r.Reposition(coordX, coordY);
        }
    }

    private string updateBuildAndMaster()
    {
        

        var scenes = new EditorBuildSettingsScene[1 + LayoutData.SceneCount()];
        scenes[0] = new EditorBuildSettingsScene(AssetDatabase.GetAssetPath(LayoutData.MasterScene), true);

        var mm = FindObjectOfType<MapManager>();
        if(mm == null)
        {
            return "Couldn't find map manager!";
        }

        Undo.RecordObject(mm, "Updating scene list");

        mm.Scenes = new List<PlacedScene>();
        int index = 1;
        foreach(var cs in LayoutData.SubScenes)
        {
            scenes[index] = new EditorBuildSettingsScene(AssetDatabase.GetAssetPath(cs.Scene), true);

            var ps = new PlacedScene();
            ps.Index = index;
            ps.X = cs.X;
            ps.Y = cs.Y;
            mm.Scenes.Add(ps);

            index++;
        }

        EditorBuildSettings.scenes = scenes;

        

        return "Built with " + scenes.Length.ToString() + " scenes.";
    }

    private void updateMinMax()
    {

    }

    private bool tryAddScene()
    {
        if(selectedScene == null)
        {
            return false;
        }

        //check that we don't already have the selected scene included in the list
        foreach(var cs in LayoutData.SubScenes)
        {
            if(selectedScene == cs.Scene)
            {
                return false;
            }
        }

        CoordScene add = new CoordScene();
        add.X = coordX;
        add.Y = coordY;
        add.Scene = selectedScene;

        Undo.RecordObject(LayoutData, "Adding scene");


        LayoutData.SubScenes.Add(add);

        EditorUtility.SetDirty(LayoutData);

        resetEditor();

        return true;
    }
}