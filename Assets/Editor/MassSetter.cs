﻿using UnityEngine;
using UnityEditor;

public class MassSetter : EditorWindow
{
    [MenuItem("Tools/Mass setter")]
    static void Init()
    {
        GetWindow(typeof(MassSetter)).Show();
    }

    private float totalmass;

    void OnGUI()
    {
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();


        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.TextField("words");
        EditorGUILayout.TextField("words");
        EditorGUILayout.TextField("words");
        EditorGUILayout.TextField("words");
        EditorGUILayout.Toggle(false, GUILayout.Width(20));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        totalmass = EditorGUILayout.FloatField("Total mass:", totalmass);

        if(GUILayout.Button("Set mass"))
        {
            setMass();
        }
    }

    private void setMass()
    {
        var totalvolume = 0f;

        foreach(var s in Selection.gameObjects)
        {
            var rb = s.GetComponent<Rigidbody>();
            if(rb != null)
            {
                rb.SetDensity(1);
                totalvolume += rb.mass;
            }
        }

        foreach(var s in Selection.gameObjects)
        {
            var rb = s.GetComponent<Rigidbody>();
            if(rb != null)
            {
                rb.mass = totalmass * (rb.mass / totalvolume);
            }
        }
    }
}