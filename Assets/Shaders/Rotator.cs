﻿using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float HRate;
    public float VRate;
    public float DRate;

    void Update()
    {
        transform.Rotate(Vector3.up, HRate * Time.deltaTime, Space.World);
        transform.Rotate(Vector3.right, VRate * Time.deltaTime, Space.World);
        transform.Rotate(Vector3.forward, DRate * Time.deltaTime, Space.Self);
    }
}