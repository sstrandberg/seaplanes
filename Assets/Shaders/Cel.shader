﻿Shader "Custom/CelShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0

		_ThresholdGloss("Threshold for gloss", Range(0,1)) = 0.8
		_ThresholdBright("Threshold for bright", Range(0,1)) = 0.5

		_ColorGloss("Color gloss", Color) = (1,1,1,1)
		_ColorBright("Color bright", Color) = (1,1,1,1)
		_ColorDark("COlor dark", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows finalcolor:fcolor

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		float _ThresholdGloss;
		float _ThresholdBright;
		
		fixed4 _ColorGloss;
		fixed4 _ColorBright;
		fixed4 _ColorDark;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_CBUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}

		void fcolor(Input IN, SurfaceOutputStandard o, inout fixed4 col){
			if(col.r > _ThresholdGloss) col = _ColorGloss;
			else if(col.r > _ThresholdBright) col = _ColorBright;
			else col = _ColorDark;
		}

		ENDCG
	}
	FallBack "Diffuse"
}
