﻿Shader "Custom/PropellerFast" {
	Properties {
		_Fade("Fadeout", float) = 1
		_MainTex ("Noise texture", 2D) = "white" {}
		_PrimaryColor("Primary color", color) = (1,1,1,1)
		_SecondaryColor("Secondary color", color) = (0,0,0,0)
		_StripeCount("Stripe count", float) = 10
		_ArcCount("Arc count", float) = 2
		_RandomColumn("Random column", float) = 0
		_ArcSize("Arc size", range(0,1)) = 0.3
		_ArcSoftness("Arc softness", range(0,1)) = 0.1
		_ArcPositionShift("Arc position shift", range(0, 0.5)) = 0.1
		_ArcSizeShift("Arc size shift", range(0, 0.5)) = 0.1
	}
	SubShader {
		 Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		 Cull off
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf MatchedNormal fullforwardshadows alpha:fade 

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		fixed _Fade;

		fixed4 _PrimaryColor;
		fixed4 _SecondaryColor;

		float _StripeCount;
		float _ArcCount;

		float _ArcSize;
		float _ArcSoftness;

		float _ArcPositionShift;
		float _ArcSizeShift;

		float _RandomColumn;


		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_CBUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_CBUFFER_END

		half4 LightingMatchedNormal(SurfaceOutput s, half3 lightDir, half atten){
			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * atten;
			c.a = s.Alpha;
			return c;
		}

		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color

			float2 samplePos = float2(_Time.x * 100 + floor(IN.uv_MainTex.x * _ArcCount + 0.5) * (1 / _ArcCount), floor(IN.uv_MainTex.y * _StripeCount) / 128);
			fixed4 noiseSample = tex2D(_MainTex, samplePos);

			float stripeValue = pow(1 - abs(frac(IN.uv_MainTex.y * _StripeCount) - 0.5) * 2, 4);

			float arcPosShift = lerp(-_ArcPositionShift, _ArcPositionShift, noiseSample.x);
			float arcSizeShift = lerp(-_ArcSizeShift, _ArcSizeShift, noiseSample.y);
			float arcDistanceTrue = (frac(IN.uv_MainTex.x * _ArcCount + arcPosShift) - 0.5) * 2;
			float arcPos = abs(arcDistanceTrue);

			float arcValue = saturate((arcPos - (1 - (_ArcSize + arcSizeShift + _ArcSoftness / 2))) / (_ArcSoftness));


			fixed4 col = lerp(_SecondaryColor, _PrimaryColor, stripeValue * arcValue);
			//o.Alpha = stripeValue * arcValue;
			o.Albedo = col.rgb;
			o.Alpha = col.a * _Fade;
		}
		ENDCG
	}
	FallBack "Diffuse"
}