﻿Shader "Custom/UnderwaterShader" {
	Properties {
            _MainTex ("Base (RGB)", 2D) = "white" {}
            _Color("Color", color) = (1,1,1,1)
			_MaxDepth("Max depth", float) = 10
			_DepthPower("Depth power", float) = 2
			_Tess ("Tessellation", Range(1,32)) = 4
        }
        SubShader {
            Tags { "RenderType"="Opaque" "Queue"="Transparent"}
            LOD 300

			ZWrite Off
			ZTest Greater
			Stencil{
				Ref 1
				Comp Equal
			}
            
            CGPROGRAM
            #pragma surface surf BlinnPhong vertex:disp tessellate:tessFixed nolightmap
            #pragma target 4.6

			

            struct appdata {
                float4 vertex : POSITION;
                float4 tangent : TANGENT;
                float3 normal : NORMAL;
            };

            float _Tess;

            float4 tessFixed()
            {
                return _Tess;
            }

            void disp (inout appdata v)
            {
                //v.vertex.xyz += v.normal * d;
            }

            struct Input {
                float2 uv_MainTex;
				float3 worldPos;
            };

            sampler2D _MainTex;
            sampler2D _NormalMap;
            fixed4 _Color;

            void surf (Input IN, inout SurfaceOutput o) {
                o.Albedo = float4(0,0,0,0.5);
            }
            ENDCG
        }
        FallBack "Diffuse"
    }