﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Water/StencilBufferMasked"
{
	Properties
	{
		_Color("Color", color) = (1,1,1,1)
		_MaxDepth("Max depth", float) = 10
		_DepthPower("Depth power", float) = 2
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue"="Transparent"}
		LOD 100

		Pass
		{
			ZWrite Off
			ZTest Greater
			Blend SrcAlpha OneMinusSrcAlpha

			Stencil{
				Ref 1
				Comp Equal
				//Comp Always
				//Pass keep
			}

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				
			};			

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float propdepth : TEXCOORD1;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			fixed4 _Color;
			float _MaxDepth;
			float _DepthPower;
			
			float4 tessFixed()
            {
                return 4;
            }

			v2f vert (appdata v)
			{
				v2f o;
				
				float4 pos = mul(unity_ObjectToWorld, v.vertex);
				float4 adjustedpos = pos;
				adjustedpos.y *= 3.0/4;

				float3 dirToCam = _WorldSpaceCameraPos - pos;
				float3 surfacePos = pos + dirToCam * (-pos.y /dirToCam.y);
				//float wiggle = sin(_Time.y * 2 + surfacePos.x * 1) * adjustedpos.y * 0.5;
				float wiggle = 0;

				o.uv = float2(1,1);

				o.vertex = UnityObjectToClipPos(mul(unity_WorldToObject, adjustedpos)) + float4(0,wiggle, 0, 0);

				o.propdepth = -pos.y / _MaxDepth;

				//o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				//fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 col = _Color;
				col.a = pow(1 - saturate(i.propdepth), _DepthPower);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
