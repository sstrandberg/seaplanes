﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Unlit/Outline"
{
	Properties
	{
		_Color ("Color", color) = (1,1,1,1)
		_ZOffset ("Z offset", float) = 0.0005
		_DebugFiddle("Debug fiddle", float) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100
		ZWrite On
		//ZTest Always
		Offset 0, -1
		Stencil {
			Ref 2
			Comp Always
			Pass replace
		}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert  
			#pragma fragment frag 
 
			#include "UnityCG.cginc"
 
			uniform float4 _Color; // define shader property for shaders
			uniform float _ZOffset;
			uniform float _DebugFiddle;
 
			struct vertexInput {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 tangent : TANGENT;
			};
			struct vertexOutput {
				float4 pos : SV_POSITION;
				float3 normalA : TEXCOORD;
				float3 normalB : TEXCOORD1;
				float3 viewDir : TEXCOORD2;
			};
 
			vertexOutput vert(vertexInput input) {
				vertexOutput output;
 
				float4x4 modelMatrix = unity_ObjectToWorld;
				float4x4 modelMatrixInverse = unity_WorldToObject; 
 
				output.normalA = normalize(
				   mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
				output.normalB = normalize(
				   mul(float4(input.tangent.xyz, 0.0), modelMatrixInverse).xyz);
				output.viewDir = normalize(_WorldSpaceCameraPos 
				   - mul(modelMatrix, input.vertex).xyz);

				output.pos = UnityObjectToClipPos(input.vertex);
				output.pos.z += _ZOffset;
				return output;
			 }
 
			 float4 frag(vertexOutput input) : COLOR
			 {
				float3 normA = normalize(input.normalA);
				float3 normB = normalize(input.normalB);

				float3 viewDir = normalize(input.viewDir);

				//clip(-1 * dot(normA, viewDir) * dot(normB, viewDir));

				clip(-1 * dot(normA, viewDir) * dot(normB, viewDir) + _DebugFiddle);

				float4 col = float4(_Color);

				return col;
			 }
 
         ENDCG
      }
	}
}
