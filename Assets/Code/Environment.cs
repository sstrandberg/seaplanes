﻿using UnityEngine;

public class Environment : MonoBehaviour
{


    public float DEBUGALTITUDE;
    public float DEBUGDENSITY;

    public float AirTemperature = 20;

    private static Environment cachedInstance;

    public static Environment Instance
    {
        get
        {
            if (cachedInstance == null)
            {
                cachedInstance = FindObjectOfType<Environment>();
            }

            return cachedInstance;
        }
    }

    public float WindSpeed;
    public float WindDirection;

    /// <summary>
    /// Wind velocity in m/s
    /// </summary>
    public Vector3 WindVelocity
    {
        get
        {
            return Quaternion.AngleAxis(WindDirection + 180, Vector3.up) * Vector3.forward * (WindSpeed * Mathx.KnotsToMS);
        }
    }

    public float TideSpeed;
    public float TideDirection;

    /// <summary>
    /// Tide velocity in m/s
    /// </summary>
    public Vector3 TideVelocity
    {
        get
        {
            return Quaternion.AngleAxis(TideDirection, Vector3.up) * Vector3.forward * (TideSpeed * Mathx.KnotsToMS);

        }
    }

    private float[] referencealtitudes;
    private float[] densities;
    private float[] standardtemperatures;
    private float[] temperatureLapseRate;


    private void OnValidate()
    {
        

        DEBUGDENSITY = AirDensity(DEBUGALTITUDE);
    }


    //https://en.wikipedia.org/wiki/Barometric_formula
    public float AirDensity(float altitude)
    {
        checkTables();
        if (altitude < 0) altitude = 0;

        var altindex = 6;
        for (int i = 0; i < 7; i++)
        {
            if (referencealtitudes[i] > altitude)
            {
                altindex = i - 1;
                break;
            }
        }
        if (altindex < 0) altindex = 0;

        var g = 9.80665f;
        var M = 0.0289644f;
        var R = 8.3144598f;

        return densities[altindex]
            * Mathf.Exp((-g * M * (altitude - referencealtitudes[altindex]))
            / (R * standardtemperatures[altindex]));
    }

    private void checkTables()
    {
        if (referencealtitudes == null || referencealtitudes.Length != 7)
        {
            referencealtitudes = new float[7]
            {
                0,
                11000,
                20000,
                32000,
                47000,
                51000,
                71000
            };
            densities = new float[7]
            {
                1.2250f,
                0.36391f,
                0.08803f,
                0.01322f,
                0.00143f,
                0.00086f,
                0.000064f
            };
            standardtemperatures = new float[7]
            {
                288.15f,
                216.65f,
                216.65f,
                228.65f,
                270.65f,
                270.65f,
                214.65f
            };
            temperatureLapseRate = new float[7]
            {
                -0.0065f,
                0.0f,
                0.001f,
                0.0028f,
                0f,
                -0.0028f,
                -0.002f
            };
        }
    }
}