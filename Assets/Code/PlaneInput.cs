﻿using UnityEngine;
using InControl;


public class PlaneInput : MonoBehaviour
{

    private PlaneManager pc;

    private static PlaneActionSet actions;

    void Start()
    {
        setUpActions();
        pc = GetComponent<PlaneManager>();
    }

    void Update()
    {
        if (actions == null)
        {
            setUpActions();
        }
        else
        {
            InputManager.AttachPlayerActionSet(actions);
        }

        pc.RollAxis = actions.Roll.Value;
        pc.PitchAxis = actions.Pitch.Value;
        pc.YawAxis = actions.Yaw.Value;

        pc.Throttle = Mathf.Clamp01(pc.Throttle + actions.ThrottleControl.Value * Time.deltaTime);

        if (actions.PitchTrimUp.WasPressed)
        {
            pc.PitchTrim += 0.01f;
        }
        if (actions.PitchTrimDown.WasPressed)
        {
            pc.PitchTrim -= 0.01f;
        }

        pc.Shooting = actions.Shoot.IsPressed;
    }


    private static void setUpActions()
    {
        actions = new PlaneActionSet();
        actions.RollLeft.AddDefaultBinding(InputControlType.LeftStickLeft);
        actions.RollRight.AddDefaultBinding(InputControlType.LeftStickRight);
        actions.PitchUp.AddDefaultBinding(InputControlType.LeftStickDown);
        actions.PitchDown.AddDefaultBinding(InputControlType.LeftStickUp);
        actions.PitchTrimUp.AddDefaultBinding(InputControlType.DPadDown);
        actions.PitchTrimDown.AddDefaultBinding(InputControlType.DPadUp);
        actions.YawLeft.AddDefaultBinding(InputControlType.RightStickLeft);
        actions.YawRight.AddDefaultBinding(InputControlType.RightStickRight);

        actions.ThrottleDown.AddDefaultBinding(InputControlType.LeftTrigger);
        actions.ThrottleUp.AddDefaultBinding(InputControlType.RightTrigger);



        actions.Shoot.AddDefaultBinding(InputControlType.LeftBumper);
    }
}

public class PlaneActionSet : PlayerActionSet
{
    public PlayerAction RollLeft;
    public PlayerAction RollRight;
    public PlayerOneAxisAction Roll;
    public PlayerAction PitchUp;
    public PlayerAction PitchDown;
    public PlayerAction PitchTrimUp;
    public PlayerAction PitchTrimDown;

    public PlayerOneAxisAction Pitch;
    public PlayerAction YawLeft;
    public PlayerAction YawRight;
    public PlayerOneAxisAction Yaw;

    public PlayerAction Shoot;

    public PlayerAction ThrottleUp;
    public PlayerAction ThrottleDown;
    public PlayerOneAxisAction ThrottleControl;


    public PlaneActionSet()
    {
        RollLeft = CreatePlayerAction("Roll left");
        RollRight = CreatePlayerAction("Roll right");
        Roll = CreateOneAxisPlayerAction(RollLeft, RollRight);

        PitchUp = CreatePlayerAction("Pitch up");
        PitchDown = CreatePlayerAction("Pitch down");
        Pitch = CreateOneAxisPlayerAction(PitchDown, PitchUp);

        PitchTrimUp = CreatePlayerAction("Pitch trim up");
        PitchTrimDown = CreatePlayerAction("Pitch trim down");

        YawLeft = CreatePlayerAction("Yaw left");
        YawRight = CreatePlayerAction("Yaw right");
        Yaw = CreateOneAxisPlayerAction(YawLeft, YawRight);

        ThrottleUp = CreatePlayerAction("Throttle up");
        ThrottleDown = CreatePlayerAction("Throttle down");
        ThrottleControl = CreateOneAxisPlayerAction(ThrottleDown, ThrottleUp);

        Shoot = CreatePlayerAction("Shoot");
    }
}
