﻿using UnityEngine;

public class ZeroBody : MonoBehaviour
{
    public float Mass = 1;
    public bool UseGravity;
    [Range(0, 1)]
    public float Bounciness;
    [Range(0, 1)]
    public float BounceDecay;
    public Vector3 Velocity;
    public Vector3 Position;

    public LayerMask Mask;

    public bool MatchOrientation;

    public void AddForce(Vector3 force, ForceMode mode)
    {
        if (Mass < 0.001f) Mass = 0.001f;

        var f = force;
        if (mode == ForceMode.Acceleration || mode == ForceMode.Force)
        {
            f *= Time.fixedDeltaTime;
        }
        if (mode == ForceMode.Force || mode == ForceMode.Impulse)
        {
            f /= Mass;
        }

        Velocity += f;
    }

    public void FixedUpdate()
    {

        Position = transform.position;

        if (UseGravity)
        {
            AddForce(Physics.gravity, ForceMode.Acceleration);
        }


        var timeLeft = Time.fixedDeltaTime;

        var looping = true;
        int count = 0;
        while (looping)
        {
            if (count++ > 5) break;
            if (timeLeft < 0) break;

            var subFrameMovement = Velocity * timeLeft;

            Ray ray = new Ray(Position, subFrameMovement.normalized);
            RaycastHit hit;

            int mask = Mask.value;
            if (Physics.Raycast(ray, out hit, subFrameMovement.magnitude, mask))
            {
                var dist = (Position - hit.point).magnitude;
                var t = dist / Velocity.magnitude;
                timeLeft -= t;

                var oldVelocity = Velocity;

                bool hitRB = false;

                var rb = hit.collider.attachedRigidbody;

                //var modifiedNormal = (Random.insideUnitSphere * 0.5f + hit.normal).normalized;
                //if (Vector3.Dot(modifiedNormal, hit.normal) < 0) modifiedNormal *= -1;

                var modifiedNormal = hit.normal;

                if (rb != null)
                {


                    hitRB = true;

                    //bounce transferred depends on dot of vel and normal

                    

                    var transfer = Vector3.Dot(Velocity.normalized, -1 * modifiedNormal);

                    var collisionVelocity = modifiedNormal * (Vector3.Dot(Velocity, modifiedNormal) + Vector3.Dot(rb.velocity * -1, modifiedNormal));

                    var ownColVel = modifiedNormal * Vector3.Dot(Velocity, modifiedNormal);
                    var othColVel = modifiedNormal * Vector3.Dot(rb.velocity, modifiedNormal);

                    var otherVel = rb.GetPointVelocity(hit.point);

                    //stick

                    var weightedAverage = (Velocity * Mass + otherVel * rb.mass) / (Mass + rb.mass);

                    Vector3 ownDelta = weightedAverage - Velocity;
                    Vector3 othDelta = weightedAverage - otherVel;

                    Velocity += ownDelta * (1 - Bounciness);
                    rb.AddForceAtPosition(othDelta * (1 - Bounciness), hit.point, ForceMode.VelocityChange);


                    //bounce
                    var bounceMomentum = (-2 * ownColVel) * Bounciness * Mass;
                    AddForce(bounceMomentum, ForceMode.Impulse);
                    rb.AddForceAtPosition(-1 * bounceMomentum, hit.point, ForceMode.Impulse);
                }
                else
                {

                    var ownColVel = modifiedNormal * Vector3.Dot(Velocity, modifiedNormal);
                    var weightedAverage = ownColVel;
                    var ownDelta = weightedAverage - ownColVel;
                    Velocity += ownDelta * (1 - Bounciness);

                    var bounceMomentum = (-2 * ownColVel) * Bounciness * Mass;
                    AddForce(bounceMomentum, ForceMode.Impulse);
                    //Velocity = Vector3.Reflect(Velocity, hit.normal) * Bounciness;
                }

                //Velocity -= Velocity.normalized * Velocity.sqrMagnitude * BounceDecay;
                Velocity *= (1 - BounceDecay);
                var impulse = (Velocity - oldVelocity) * Mass;

                ZBCollisionInfo c = new ZBCollisionInfo(this, hit.collider, hit.point, impulse);
                SendMessage("OnZeroBodyCollision", c, SendMessageOptions.DontRequireReceiver);

                if (rb != null)
                {
                    rb.gameObject.SendMessage("OnZeroBodyCollision", c, SendMessageOptions.DontRequireReceiver);
                }
                else
                {
                    hit.collider.gameObject.SendMessage("OnZeroBodyCollision", c, SendMessageOptions.DontRequireReceiver);
                }
            }
            else
            {
                Position = Position + subFrameMovement;
                looping = false;
            }
        }

        transform.position = Position;

        if (MatchOrientation)
        {
            transform.rotation = Quaternion.LookRotation(Velocity.normalized);
        }

        //loop:
        //check where we want to go
        //check for collisions
        //  if not just go there
        //otherwise:
        //  calculate time taken to go to the collision spot
        //  modify velocity bounce and stuff
        //  go to collision spot
        //  go again
    }
}

public class ZBCollisionInfo
{
    public ZeroBody ZeroBody;
    public Collider Collider;
    public Vector3 Point;
    public Vector3 Impulse;

    public ZBCollisionInfo(ZeroBody zb, Collider other, Vector3 point, Vector3 impulse)
    {
        ZeroBody = zb;
        Collider = other;
        Point = point;
        Impulse = impulse;
    }
}