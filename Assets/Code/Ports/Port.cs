﻿using UnityEngine;

public class Port : MonoBehaviour
{
    public string PortName;

    public Vector3 MainCameraLocation;
    public Vector3 MainCameraLookAt;

    public Vector3 SpawnLocation;

    public Vector3 LandingZoneOrigin;
    public Vector3 LandingZoneScale;
    public float LandingZoneRotation;

    public Camera Camera;

    public Matrix4x4 LandingZoneMatrix
    {
        get
        {
            return Matrix4x4.TRS(transform.TransformPoint(LandingZoneOrigin),
            Quaternion.AngleAxis(LandingZoneRotation, Vector3.up), LandingZoneScale);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.TransformPoint(MainCameraLocation), 2);
        Gizmos.DrawLine(transform.TransformPoint(MainCameraLocation),
            transform.TransformPoint(MainCameraLookAt));

        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.TransformPoint(SpawnLocation), 2);

        Gizmos.matrix = LandingZoneMatrix;
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(Vector3.zero, Vector3.one);

    }

    void OnValidate()
    {
        var menuCam = GetComponentInChildren<Camera>();
        if(menuCam != null)
        {
            menuCam.transform.position = transform.TransformPoint(MainCameraLocation);
            menuCam.transform.LookAt(transform.TransformPoint(MainCameraLookAt));
        }
    }
}