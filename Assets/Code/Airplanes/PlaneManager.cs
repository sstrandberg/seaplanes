﻿using UnityEngine;
using System.Collections.Generic;

public class PlaneManager : MonoBehaviour
{

    [Range(-1f, 1f)]
    public float YawAxis;
    public float YawTrim;
    public float YawControl
    {
        get
        {
            return Mathf.Clamp(YawAxis + YawTrim, -1, 1);
        }
    }

    [Range(-1f, 1f)]
    public float PitchAxis;
    public float PitchTrim;
    public float PitchControl
    {
        get
        {
            return Mathf.Clamp(PitchAxis + PitchTrim, -1, 1);
        }
    }

    [Range(-1f, 1f)]
    public float RollAxis;
    public float RollTrim;
    public float RollControl
    {
        get
        {
            return Mathf.Clamp(RollAxis + RollTrim, -1, 1);
        }
    }

    [Range(0f, 1f)]
    public float Throttle;

    private Wing[] wings;

    public bool Shooting;


    public float TotalMass;

    void OnValidate()
    {
        TotalMass = 0;
        foreach(var rb in GetComponentsInChildren<Rigidbody>())
        {
            TotalMass += rb.mass;
        }
    }

    void Update()
    {
        var resetspeed = 0f;
        YawAxis = Mathf.MoveTowards(YawAxis, 0, resetspeed * Time.deltaTime);
        PitchAxis = Mathf.MoveTowards(PitchAxis, 0, resetspeed * Time.deltaTime);
        RollAxis = Mathf.MoveTowards(RollAxis, 0, resetspeed * Time.deltaTime);
    }



    void Awake()
    {
        wings = new Wing[0];
        //wings = GetComponentsInChildren<Wing>();
    }


    public void AddWing(Wing w)
    {
        var l = new List<Wing>(wings);
        l.Add(w);
        wings = l.ToArray();
    }

    void FixedUpdate()
    {
        for (int j = 0; j < wings.Length; j++)
        {
            wings[j].CalcForces();
        }
        for (int j = 0; j < wings.Length; j++)
        {
            wings[j].AddForces(1);
        }
    }
}