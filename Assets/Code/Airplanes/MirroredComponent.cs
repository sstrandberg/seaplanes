﻿using UnityEngine;

[ExecuteInEditMode]
public class MirroredComponent : MonoBehaviour
{
    public GameObject Target;
    
    void Update()
    {

        if (Application.isPlaying) return;

        if (Target == null) return;

        var pos = Target.transform.localPosition;
        var rot = Target.transform.localEulerAngles;
        var scale = Target.transform.localScale;

        pos.x *= -1;
        rot.y *= -1;
        rot.z *= -1;

        transform.localPosition = pos;
        transform.localEulerAngles = rot;
        transform.localScale = scale;

        if (Target.GetComponent<Wing>())
        {
            var wingTarget = Target.GetComponent<Wing>();

            Wing wing = GetComponent<Wing>();
            if (wing == null) wing = gameObject.AddComponent<Wing>();

            wing.Parent = wingTarget.Parent;
            wing.Foil = wingTarget.Foil;

            wing.Mirror = !wingTarget.Mirror;
            wing.Sections = wingTarget.Sections;
            wing.InnerWidth = wingTarget.InnerWidth;
            wing.OuterWidth = wingTarget.OuterWidth;
            wing.Length = wingTarget.Length;
            wing.Sweep = wingTarget.Sweep;
            wing.Twist = wingTarget.Twist;

            wing.HasControlSurface = wingTarget.HasControlSurface;
            wing.ControlSurfaceType = wingTarget.ControlSurfaceType;
            wing.ControlSurfaceSectionStart = wingTarget.ControlSurfaceSectionStart;
            wing.ControlSurfaceSectionEnd = wingTarget.ControlSurfaceSectionEnd;
            wing.ControlSurfaceProportion = wingTarget.ControlSurfaceProportion;
            wing.ControlSurfaceMinAngle = wingTarget.ControlSurfaceMinAngle;
            wing.ControlSurfaceMaxAngle = wingTarget.ControlSurfaceMaxAngle;
        }


        if (Target.GetComponent<Rigidbody>())
        {
            var trb = Target.GetComponent<Rigidbody>();

            var rb = GetComponent<Rigidbody>();
            if (rb == null) rb = gameObject.AddComponent<Rigidbody>();

            rb.mass = trb.mass;
        }
    }
}