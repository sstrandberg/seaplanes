﻿using UnityEngine;
using System.Collections.Generic;

public class OutlineGenerator : MonoBehaviour
{
    public SkinnedMeshRenderer SMRSource;
    public Mesh Source;

    public bool UseSkinnedMeshRenderer;

    [ContextMenu("Generate mesh")]
    public void GenerateMesh()
    {

        var mesh = new Mesh();

        AnalyzedMesh am;
        if(Source == null && !UseSkinnedMeshRenderer)
        {
            am = new AnalyzedMesh(GetComponent<MeshFilter>().sharedMesh, transform);
        }
        else
        {
            am = new AnalyzedMesh(Source, transform);
        }
        

        //mesh.name = Source.name + " (outline)";

        var verts = new List<Vector3>();
        var normals = new List<Vector3>();
        var tangents = new List<Vector4>();
        var weights = new List<BoneWeight>();

        foreach (var p in am.LinePairs)
        {
            var bonusweight = 0.2f;

            verts.Add(am.Vertices[p.IndexA]);
            verts.Add(am.Vertices[p.IndexB]);
            normals.Add((p.LeftNormal + p.LeftNormalExtraA * bonusweight).normalized);
            normals.Add((p.LeftNormal + p.LeftNormalExtraB * bonusweight).normalized);
            tangents.Add((p.RightNormal + p.RightNormalExtraA * bonusweight).normalized);
            tangents.Add((p.RightNormal + p.RightNormalExtraB * bonusweight).normalized);

            if (UseSkinnedMeshRenderer)
            {
                weights.Add(am.BoneWeights[p.IndexA]);
                weights.Add(am.BoneWeights[p.IndexB]);
            }


            var indices = new int[verts.Count];

            for (int i = 0; i < indices.Length; i++)
            {
                indices[i] = i;
            }

            mesh.SetVertices(verts);
            mesh.SetNormals(normals);
            mesh.SetTangents(tangents);
            mesh.SetIndices(indices, MeshTopology.Lines, 0);

            if (UseSkinnedMeshRenderer)
            {
                mesh.boneWeights = weights.ToArray();
                mesh.bindposes = Source.bindposes;
            }

            if (!UseSkinnedMeshRenderer)
            {
                GetComponent<MeshFilter>().sharedMesh = mesh;
            }
            else
            {
                var smr = GetComponent<SkinnedMeshRenderer>();
                GetComponent<SkinnedMeshRenderer>().sharedMesh = mesh;
                //smr.m
                //smr.sharedMesh = Source;
            }
        }
    }
}