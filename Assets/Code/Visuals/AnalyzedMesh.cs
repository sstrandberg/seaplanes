﻿using UnityEngine;
using System.Collections.Generic;

public class AnalyzedMesh
{
    public Vector3[] Vertices;
    public Vector3[] Normals;
    public Vector4[] Tangents;
    public Color[] Colors;
    public Vector2[] UV0;
    public Vector2[] UV1;
    public Vector2[] UV2;
    public Vector2[] UV3;
    public BoneWeight[] BoneWeights;

    public int SubmeshCount;
    public int[][] Indices;

    public List<Triangle> Triangles;
    public List<LinePair> LinePairs;

    public Transform DebugSpace;

    public AnalyzedMesh(Mesh m, Transform debugspace)
    {
        DebugSpace = debugspace;
        Vertices = m.vertices;

        Normals = m.normals;
        Tangents = m.tangents;
        Colors = m.colors;
        UV0 = m.uv;
        UV1 = m.uv2;
        UV2 = m.uv3;
        UV3 = m.uv4;
        BoneWeights = m.boneWeights;

        SubmeshCount = m.subMeshCount;
        Indices = new int[SubmeshCount][];
        for (int i = 0; i < SubmeshCount; i++)
        {
            Indices[i] = m.GetIndices(i);
        }


        Triangles = new List<Triangle>();
        for (int i = 0; i < SubmeshCount; i++)
        {
            for (int j = 0; j < Indices[i].Length; j += 3)
            {
                Triangles.Add(
                    new Triangle(i,
                    Indices[i][j + 0],
                    Indices[i][j + 1],
                    Indices[i][j + 2]));
            }
        }

        foreach (var t in Triangles)
        {
            t.SetInfo(this);
        }

        LinePairs = new List<LinePair>();
        foreach (var t in Triangles)
        {
            for (int i = 0; i < 3; i++)
            {
                int a = 0;
                int b = 0;
                Triangle otherTriangle = null;
                switch (i)
                {
                    case 0:
                        a = t.IndexA;
                        b = t.IndexB;
                        otherTriangle = t.NeighborAB;
                        break;
                    case 1:
                        a = t.IndexB;
                        b = t.IndexC;
                        otherTriangle = t.NeighborBC;
                        break;
                    case 2:
                        a = t.IndexC;
                        b = t.IndexA;
                        otherTriangle = t.NeighborCA;
                        break;
                }

                //if (otherTriangle == null) continue;

                bool contains = false;
                foreach (var p in LinePairs)
                {
                    if (p.Matches(a, b))
                    {
                        contains = true;
                        break;
                    }
                }
                if (contains) continue;

                LinePairs.Add(new LinePair(a, b, t, otherTriangle, this));
            }
        }

        foreach(var lp in LinePairs)
        {
            lp.FigureOutExtraNormals(this);
        }
    }

    public class Triangle
    {
        public int Submesh;
        public int IndexA;
        public int IndexB;
        public int IndexC;

        public Vector3 Normal;

        public Triangle NeighborAB;
        public Triangle NeighborBC;
        public Triangle NeighborCA;

        public Triangle(int submesh, int a, int b, int c)
        {
            Submesh = submesh;
            IndexA = a;
            IndexB = b;
            IndexC = c;
        }

        public void SetInfo(AnalyzedMesh m)
        {

            var v0 = m.Vertices[IndexA];
            var v1 = m.Vertices[IndexB];
            var v2 = m.Vertices[IndexC];

            Normal = Vector3.Cross((v1 - v0).normalized, (v2 - v0).normalized).normalized;

            if (Random.value < 0.05f)
            {
                Color c = Color.blue;
                if (Normal == Vector3.zero)
                {
                    c = Color.red;
                }
            }


            var averageposition = (v0 + v1 + v2) / 3;

            //Debug.DrawLine(m.DebugSpace.TransformPoint(v0), m.DebugSpace.TransformPoint(v1), Color.blue, 15, false);

            foreach (var t in m.Triangles)
            {
                if (t != this && t.Submesh == Submesh)
                {
                    if (TryAddNeighbor(t))
                    {
                        t.TryAddNeighbor(this);
                    }
                }
            }
        }

        public bool ContainsIndices(int A, int B)
        {
            if (A == B) return false;

            return (
                (A == IndexA || A == IndexB || A == IndexC) &&
                (B == IndexA || B == IndexB || B == IndexC));
        }

        public bool TryAddNeighbor(Triangle t)
        {
            bool added = false;
            if (NeighborAB == null && t.ContainsIndices(IndexA, IndexB))
            {
                NeighborAB = t;
                added = true;
            }
            else if (NeighborBC == null && t.ContainsIndices(IndexB, IndexC))
            {
                NeighborBC = t;
                added = true;
            }
            else if (NeighborCA == null && t.ContainsIndices(IndexA, IndexC))
            {
                NeighborCA = t;
                added = true;
            }

            return added;
        }
    }

    public class LinePair
    {
        public int IndexA;
        public int IndexB;

        public Vector3 LeftNormal;
        public Vector3 RightNormal;

        public Vector3 Direction;

        public Vector3 LeftNormalExtraA;
        public Vector3 RightNormalExtraA;
        public Vector3 LeftNormalExtraB;
        public Vector3 RightNormalExtraB;

        public LinePair(int indexA, int indexB, Triangle leftTriangle, Triangle rightTriangle, AnalyzedMesh m)
        {
            IndexA = indexA;
            IndexB = indexB;

            LeftNormal = leftTriangle.Normal;
            if(rightTriangle != null)
            {
                RightNormal = rightTriangle.Normal;
            }
            else
            {
                RightNormal = -LeftNormal;
            }

            Direction = (m.Vertices[IndexB] - m.Vertices[IndexA]).normalized;
        }

        public bool Matches(int a, int b)
        {
            return (IndexA == a && IndexB == b) || (IndexA == b && IndexB == a);
        }

        public void FigureOutExtraNormals(AnalyzedMesh m)
        {
            LinePair ACandidate = null;
            LinePair BCandidate = null;
            float bestADot = 0f;
            float bestBDot = 0f;

            foreach (var lp in m.LinePairs)
            {
                if (lp == this) continue;

                if(lp.IndexA == IndexA || lp.IndexB == IndexA)
                {
                    var dot = Vector3.Dot(lp.Direction, Direction);
                    if(Mathf.Abs(dot) > bestADot)
                    {
                        bestADot = dot;
                        ACandidate = lp;
                    }
                }
                if(lp.IndexA == IndexB || lp.IndexB == IndexB)
                {
                    var dot = Vector3.Dot(lp.Direction, Direction);
                    if (Mathf.Abs(dot) > bestBDot)
                    {
                        bestBDot = Mathf.Abs(dot);
                        BCandidate = lp;
                    }
                }
            }

            if(ACandidate == null)
            {
                LeftNormalExtraA = LeftNormal;
                RightNormalExtraA = RightNormal;
            }
            else
            {
                if (Vector3.Dot(ACandidate.Direction, Direction) > 0)
                {
                    LeftNormalExtraA = ACandidate.LeftNormal;
                    RightNormalExtraA = ACandidate.RightNormal;
                }
                else
                {
                    RightNormalExtraA = ACandidate.LeftNormal;
                    LeftNormalExtraB = ACandidate.RightNormal;
                }
            }

            if (BCandidate == null)
            {
                LeftNormalExtraB = LeftNormal;
                RightNormalExtraB = RightNormal;
            }
            else
            {
                if (Vector3.Dot(BCandidate.Direction, Direction) > 0)
                {
                    LeftNormalExtraB = BCandidate.LeftNormal;
                    RightNormalExtraB = BCandidate.RightNormal;
                }
                else
                {
                    RightNormalExtraB = BCandidate.LeftNormal;
                    LeftNormalExtraB = BCandidate.RightNormal;
                }
            }
        }
    }
}