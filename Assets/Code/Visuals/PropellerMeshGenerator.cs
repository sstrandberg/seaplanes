﻿using UnityEngine;

public class PropellerMeshGenerator : MonoBehaviour
{

    public int Sections = 16;
    public float InnerRadius = 1;
    public float OuterRadius = 10;

    [ContextMenu("Generate propeller mesh")]
    public void GenerateInteriorMesh()
    {
        var mesh = new Mesh();

        var verts = new Vector3[(Sections + 1) * 2];
        var normals = new Vector3[(Sections + 1) * 2];
        var uv = new Vector2[(Sections + 1) * 2];
        var interiorTris = new int[Sections * 6];
        var exteriorLine = new int[Sections + 1];

        for (int i = 0; i < Sections + 1; i++)
        {
            var angle = 360f * ((float)i / Sections);
            var uvx = (float)i / Sections;

            //0
            //1

            verts[i * 2 + 0] = Quaternion.AngleAxis(angle, Vector3.forward) * Vector3.up * OuterRadius;
            verts[i * 2 + 1] = Quaternion.AngleAxis(angle, Vector3.forward) * Vector3.up * InnerRadius;
            uv[i * 2 + 0] = new Vector2(uvx, 0);
            uv[i * 2 + 1] = new Vector2(uvx, 1);

            //Debug.DrawLine(Vector3.zero, verts[i * 2 + 0], Color.green, 2);
        }

        for (int i = 0; i < normals.Length; i++)
        {
            normals[i] = Vector3.forward;
        }

        for (int i = 0; i < Sections; i++)
        {
            //02
            //13

            interiorTris[i * 6 + 0] = 2 * i + 0;
            interiorTris[i * 6 + 1] = 2 * i + 2;
            interiorTris[i * 6 + 2] = 2 * i + 1;

            interiorTris[i * 6 + 3] = 2 * i + 1;
            interiorTris[i * 6 + 4] = 2 * i + 2;
            interiorTris[i * 6 + 5] = 2 * i + 3;
        }

        for (int i = 0; i < Sections + 1; i++)
        {
            exteriorLine[i] = i * 2;
        }

        mesh.vertices = verts;
        mesh.normals = normals;
        mesh.uv = uv;
        mesh.subMeshCount = 2;
        mesh.SetIndices(interiorTris, MeshTopology.Triangles, 0);
        mesh.SetIndices(exteriorLine, MeshTopology.LineStrip, 1);

        mesh.name = "Propeller mesh";

        GetComponent<MeshFilter>().sharedMesh = mesh;
    }

    [ContextMenu("Generate noise texture")]
    public void GenerateNoise()
    {
        var width = 128;
        var tex = new Texture2D(width, width);

        for (int x = 0, y = 0; y < width; Iterate.XY(ref x, ref y, width))
        {
            var c = new Color(Random.value, Random.value, Random.value, Random.value);
            tex.SetPixel(x, y, c);
        }

        tex.Apply();


        var path = Application.dataPath + "/../Assets/PropNoise";
        byte[] bytes = tex.EncodeToPNG();
        System.IO.File.WriteAllBytes(path + ".png", bytes);

        //AssetDatabase.Refresh();
    }
}