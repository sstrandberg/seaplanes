﻿using UnityEngine;

public class PropVisualManager : MonoBehaviour
{
    public PropEngine Engine;


    public GameObject SlowProp;
    public GameObject FastProp;

    public float TransitionRPM = 1000;
    public float TransitionSmoothness = 100;
    public int BladeCount = 2;
    public int MaxSpokes = 8;

    private Material slowPropMaterial;
    private Material fastPropMain;
    private Material fastPropOutline;

    void Start()
    {
        slowPropMaterial = SlowProp.GetComponent<MeshRenderer>().material;
        fastPropMain = FastProp.GetComponent<MeshRenderer>().materials[0];
        fastPropOutline = FastProp.GetComponent<MeshRenderer>().materials[1];
    }

    void Update()
    {
        var rpf = Engine.RPM / 60 / 60;

        var smallestError = (0.5f + rpf) % 1 - 0.5f;
        var spokeCount = BladeCount;
        for (int i = MaxSpokes; i > 0; i--)
        {
            if (i % BladeCount != 0) continue;

            var fraction = 1 / (float)i;
            var error = (0.5f * fraction + rpf) % fraction - 0.5f * fraction;
            if (Mathf.Abs(error) * (float)i < Mathf.Abs(smallestError) + Mathf.Epsilon)
            {
                smallestError = error * (float)i;
                spokeCount = i;
            }
        }

        //if (spokeCount % BladeCount != 0)
        //{
        //    spokeCount *= BladeCount;
        //}

        var speedyFade = Mathf.Clamp01((Engine.RPM - TransitionRPM) / (TransitionSmoothness));
        var slowColor = Color.white;
        slowColor.a = 1 - speedyFade;
        var fastRimColor = Color.white;
        fastRimColor.a = speedyFade;

        slowPropMaterial.SetColor("_Color", slowColor);
        fastPropMain.SetFloat("_ArcCount", spokeCount);
        fastPropMain.SetFloat("_ArcPositionShift", Mathf.Abs(smallestError));
        fastPropMain.SetFloat("_Fade", speedyFade);
        fastPropOutline.SetColor("_Color", fastRimColor);

        SlowProp.transform.Rotate(0, 0, 360 * (rpf % 1), Space.Self);
        FastProp.transform.Rotate(0, 0, 360 * (rpf % 1), Space.Self);
    }
}