﻿using UnityEngine;

public class DebugUI : MonoBehaviour
{
    private PlaneManager pc;
    private Rigidbody rb;
    private PropEngine engine;

    void Start()
    {
        rb = GetComponentInParent<Rigidbody>();
        pc = GetComponentInParent<PlaneManager>();
        engine = GetComponent<PropEngine>();
    }

    void OnGUI()
    {
        var pitch = 90 - Mathf.Acos(Vector3.Dot(transform.forward, Vector3.up)) * Mathf.Rad2Deg;

        GUI.color = Color.black;
        GUILayout.BeginVertical();
        GUILayout.Label("Speed: " + (rb.velocity.magnitude * 1.94384f).ToString("f1") + "kn");
        GUILayout.Label("Throttle: " + pc.Throttle.ToString("f2"));
        GUILayout.Label("Pitch trim: " + pc.PitchTrim.ToString("f2"));
        GUILayout.Label("Pitch: " + pitch.ToString("f1") + "°");
        GUILayout.Label("Altitude: " + rb.position.y.ToString("f0") + "m");
        GUILayout.Label("Air density: " + Environment.Instance.AirDensity(rb.position.y).ToString("f3"));

        GUILayout.Label("RPM " + engine.RPM.ToString("f0"));
        GUILayout.Label("Engine temp: " + engine.EngineTemperature.ToString("f1"));
        GUILayout.Label("Engine power: " + engine.OutputPower.ToString("f1"));
        GUILayout.EndVertical();
    }
}