﻿using UnityEngine;
public class SimpleEngine : MonoBehaviour
{
    public float Force = 4000;

    public Vector3 Position;

    private Rigidbody rb;
    private PlaneController pc;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        pc = GetComponent<PlaneController>();
    }

    void FixedUpdate()
    {
        rb.AddForceAtPosition(transform.forward * Force * pc.Throttle, transform.TransformPoint(Position));

    }
}