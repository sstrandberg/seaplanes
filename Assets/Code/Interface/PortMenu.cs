﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas))]
public class PortMenu : MonoBehaviour
{
    private Canvas canvas;

    public Text Title;

    void Start()
    {
        gameObject.SetActive(false);
    }

    public void LoadPort(Port p)
    {
        canvas = GetComponent<Canvas>();

        gameObject.SetActive(true);

        canvas.gameObject.SetActive(true);
        canvas.renderMode = RenderMode.ScreenSpaceCamera;
        canvas.worldCamera = p.Camera;

        Title.text = p.PortName;
    }
}