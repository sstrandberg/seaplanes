﻿using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public PortMenu PortMenu;

    public void LoadPort(Port p)
    {
        PortMenu.LoadPort(p);
    }
}