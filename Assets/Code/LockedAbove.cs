﻿using UnityEngine;

public class LockedAbove : MonoBehaviour
{
    public float MinY;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        var v = rb.position;
        if(v.y < MinY)
        {
            v.y = 10;
            rb.position = v;
        }
    }
}