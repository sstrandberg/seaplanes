﻿using UnityEngine;

public class FastCollider : MonoBehaviour
{
    public Vector3 LastPosition;
    public Vector3 CurrentPosition;
    public Vector3 Delta;

    private Rigidbody rb;

    void OnEnable()
    {
        rb = GetComponent<Rigidbody>();
        FastColliderManager.Instance.AddCollider(this);
    }

    void OnDisable()
    {
        FastColliderManager.Instance.RemoveCollider(this);
    }

    void FixedUpdate()
    {
        LastPosition = CurrentPosition;

        if (rb == null)
        {
            CurrentPosition = transform.position;
        }
        else
        {
            CurrentPosition = rb.position;
        }

        Delta = CurrentPosition - LastPosition;
    }
}