﻿using UnityEngine;
using System.Collections.Generic;

public class FastColliderManager : MonoBehaviour
{
    public static FastColliderManager Instance
    {
        get
        {
            if (instance == null) instance = FindObjectOfType<FastColliderManager>();
            return instance;
        }
    }
    private static FastColliderManager instance;

    public List<FastCollider> Colliders;

    public void AddCollider(FastCollider c)
    {
        if (Colliders == null) Colliders = new List<FastCollider>();
        Colliders.Add(c);
    }
    
    public void RemoveCollider(FastCollider c)
    {
        if (Colliders == null) Colliders = new List<FastCollider>();
        Colliders.Remove(c);
    }
}