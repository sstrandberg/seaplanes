﻿using UnityEngine;

public class TestMB : MonoBehaviour
{
    public Port Target;

    public bool Contained;

    void Update()
    {
        if (Target == null) return;

        //Contained = Mathx.BoundingBoxCheck(transform.position, Target.transform.TransformPoint(Target.LandingZoneOrigin),
        //    Quaternion.AngleAxis(Target.LandingZoneRotation, Vector3.up), Target.LandingZoneScale);
        Contained = Mathx.BoundsCheck(Target.LandingZoneMatrix, transform.position);
    }
}

