﻿using UnityEngine;

public class AltitudeControl : MonoBehaviour
{
    public float TargetVerticalSpeed = 100;
    public float Error;
    public FloatPID PID;

    private PlaneController pc;

    void Start()
    {
        pc = GetComponent<PlaneController>();
    }

    void Update()
    {
        Error = TargetVerticalSpeed - transform.position.y;
        Error = TargetVerticalSpeed - GetComponent<Rigidbody>().velocity.y;

        pc.PitchAxis = PID.Update(Error, Time.deltaTime);
    }
}