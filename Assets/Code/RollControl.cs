﻿using UnityEngine;

public class RollControl : MonoBehaviour
{
    public float Status;

    public float WantDegreesPerSecond;
    public float WantAngle;
    public float Error;
    

    private PlaneController pc;
    private Rigidbody rb;

    public FloatPID PID;

    void Start()
    {
        pc = GetComponent<PlaneController>();
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        rb = GetComponent<Rigidbody>();

        Status = rb.angularVelocity.y;
        Vector3 wantright = Vector3.ProjectOnPlane(transform.right, Vector3.up).normalized;
        Error = -Vector3.Dot(transform.up, wantright) + Mathf.Sin(WantAngle * Mathf.Deg2Rad);

        Error = Mathf.Deg2Rad * WantDegreesPerSecond - rb.angularVelocity.y;


        pc.RollAxis = PID.Update(Error, Time.deltaTime);
    }
}