﻿using UnityEngine;

[RequireComponent(typeof(ZeroBody))]
[RequireComponent(typeof(TrailRenderer))]
public class PlayerBullet : MonoBehaviour
{
    public Material[] MaterialOptions;

    private ZeroBody zb;

    public float BulletSpeed;

    private bool destroyed = false;

    void Start()
    {
        var tr = GetComponent<TrailRenderer>();
        tr.sharedMaterial = MaterialOptions[Random.Range(0, MaterialOptions.Length)];

        zb = GetComponent<ZeroBody>();
        zb.AddForce(transform.forward * BulletSpeed,  ForceMode.VelocityChange);

        string s;

        Destroy(gameObject, 2);
    }
    
    void Update()
    {

        if(!destroyed && zb.Velocity.sqrMagnitude < 0.1f)
        {
            destroy();
        }
    }

    private void destroy()
    {
        destroyed = true;
        Destroy(gameObject);
    }

    //void OnZeroBodyCollision(ZBCollisionInfo c)
    //{
    //    var e = c.Collider.GetComponent<Enemy>();
    //    if(e != null && !destroyed)
    //    {
    //        e.Damage(1);
    //        destroy();
    //    }
    //}
}