﻿using UnityEngine;

public class AccelerationBasedAutopilot : MonoBehaviour
{
    public Vector3[] Waypoints;
    private int currentwaypoint;


    public Transform Target;


    public float TestHeading;
    public float TestPitch;

    public float PitchSensitivity = 0.1f;
    public float RollSensitivity = 0.1f;

    public float MaxGs = 5;


    public FloatPID RollPID;
    public FloatPID PitchPID;

    private Rigidbody rb;
    private PlaneManager pm;

    private Vector3 lastvelocity;
    private Vector3 currentAccel;
    private Vector3 smoothedAccel;

    public float ACCELMAGNITUDE;
    public float ROLLERROR;
    public float PITCHERROR;

    public AutopilotMode Mode;

    public enum AutopilotMode
    {
        KeepHeading,
        FollowTarget,
        FollowWaypoints
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        pm = GetComponent<PlaneManager>();

        rb.position = Vector3.up * 100;
        rb.velocity = transform.forward * 50;
    }

    void OnDrawGizmos()
    {
        switch (Mode)
        {
            case AutopilotMode.FollowWaypoints:
                if (Waypoints == null) return;
                if (Waypoints.Length < 2) return;

                Gizmos.color = Color.yellow;
                for (int i = 0; i < Waypoints.Length; i++)
                {
                    Gizmos.DrawLine(Waypoints[i], Waypoints[(i + 1) % Waypoints.Length]);
                }

                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.position, Waypoints[currentwaypoint]);
                break;
        }
        
    }

    void Update()
    {
        ACCELMAGNITUDE = currentAccel.magnitude;

        //var testDir =
        //    Quaternion.AngleAxis(TestHeading, Vector3.up) *
        //    Quaternion.AngleAxis(TestPitch, Vector3.right) *
        //    Vector3.forward;

        //Debug.DrawRay(transform.position, testDir * 10, Color.green);


        //controlDirection((Target.position - transform.position).normalized);

        if(rb.position.y < 50)
        {
            controlDirection(transform.forward + Vector3.up * 0.1f);
        }
        else
        {
            switch (Mode)
            {
                case AutopilotMode.FollowWaypoints:
                    var delta = Waypoints[currentwaypoint] - transform.position;

                    if (delta.magnitude < 50)
                    {
                        currentwaypoint = ++currentwaypoint % Waypoints.Length;
                    }

                    controlDirection(delta.normalized);
                    break;
                case AutopilotMode.FollowTarget:
                    controlDirection((Target.position - transform.position).normalized);
                    break;
                case AutopilotMode.KeepHeading:
                    var dir = Quaternion.AngleAxis(TestHeading, Vector3.up) * Quaternion.AngleAxis(TestPitch, Vector3.right) * Vector3.forward;
                    Debug.DrawRay(transform.position, dir * 10, Color.gray);
                    controlDirection(dir);
                    break;
            }
        }        
    }

    void FixedUpdate()
    {
        updateAcceleration();
    }

    private void updateAcceleration()
    {
        var dv = rb.velocity - lastvelocity;
        currentAccel = dv / Time.fixedDeltaTime - Physics.gravity;
        smoothedAccel = Vector3.Lerp(smoothedAccel, currentAccel, 0.5f);
        lastvelocity = rb.velocity;
    }

    private void controlDirection(Vector3 direction)
    {
        var velcache = rb.velocity;
        if(rb.velocity.magnitude < 1)
        {
            rb.velocity = Vector3.forward * 0.01f;
        }
        var turnDirection = Vector3.ProjectOnPlane(direction, rb.velocity).normalized;

        var turnGs = Mathx.Map(Vector3.Angle(rb.velocity, direction), 0, 30, 0, MaxGs);

        var wantaccel = (turnDirection * turnGs + Vector3.up) * 9.81f;

        Debug.DrawRay(transform.position, wantaccel, Color.blue);

        var wantRight = Vector3.Cross(wantaccel, rb.velocity).normalized;

        Debug.DrawRay(transform.position, wantRight * 10, Color.magenta);
        Debug.DrawRay(transform.position, smoothedAccel, Color.red);

        Debug.DrawRay(transform.position, rb.velocity.normalized * 10, Color.black);

        ROLLERROR = -Vector3.Dot(wantRight, transform.up) * RollSensitivity;
        PITCHERROR = (Vector3.Dot(wantaccel, transform.up) - Vector3.Dot(smoothedAccel, transform.up)) * PitchSensitivity;

        pm.RollAxis = RollPID.Update(ROLLERROR, Time.deltaTime);
        pm.PitchAxis = PitchPID.Update(PITCHERROR, Time.deltaTime);

        //figure out which direction we want to accelerate in

        //figure out which direction should be "up" (add gravity)

        //execute roll control to match direction

        //execute pitch control to match Gs


        rb.velocity = velcache;
    }
}