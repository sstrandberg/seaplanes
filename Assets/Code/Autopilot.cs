﻿using UnityEngine;

public class Autopilot : MonoBehaviour
{

    public AccelerationBasedAutopilot.AutopilotMode Mode;

    public Vector3[] Waypoints;

    public float MaxPitch = 5;
    public float MinPitch = 5;
    public float MaxAngle = 45;
    public float AngleSensitivity = 1;

    public float CruiseControlSpeed = 50;
    public float CruiseControlSensitivity = 1;
    public float DistanceToTarget = 100;
    public float DistanceSensitivity = 0.5f;


    public FloatPID VerticalSpeedPID;
    public FloatPID RotationPID;
    public FloatPID ThrottlePID;

    public Transform FollowTarget;

    public float TARGETPITCH;
    public float CURRENTPITCH;
    public float TARGETANGLEDIFFERENCE;
    public float TARGETANGLE;


    private Rigidbody rb;
    private PlaneController pc;

    private int currentwaypoint;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        pc = GetComponent<PlaneController>();
    }

    void OnDrawGizmos()
    {
        switch ((Mode))
        {
            case AccelerationBasedAutopilot.AutopilotMode.FollowWaypoints:
                if (Waypoints.Length < 2) return;

                Gizmos.color = Color.yellow;
                for (int i = 0; i < Waypoints.Length; i++)
                {
                    Gizmos.DrawLine(Waypoints[i], Waypoints[(i + 1) % Waypoints.Length]);
                }

                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.position, Waypoints[currentwaypoint]);

                break;
            case AccelerationBasedAutopilot.AutopilotMode.FollowTarget:
                if (FollowTarget == null) return;

                var d = (transform.position - FollowTarget.position).normalized;

                var p = FollowTarget.position + d * DistanceToTarget;

                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(FollowTarget.position, p);
                Gizmos.color = Color.red;
                Gizmos.DrawLine(p, transform.position);
                break;
            case AccelerationBasedAutopilot.AutopilotMode.KeepHeading:
                break;
        }
    }

    void Update()
    {
        switch (Mode)
        {
            case AccelerationBasedAutopilot.AutopilotMode.FollowWaypoints:
                updateWaypoints();
                var targetpos = Waypoints[currentwaypoint];
                flyTowardsTarget(targetpos);
                cruiseControl(CruiseControlSpeed);
                break;
            case AccelerationBasedAutopilot.AutopilotMode.FollowTarget:
                flyTowardsTarget(FollowTarget.position);
                matchDistance(FollowTarget.position, DistanceToTarget);
                break;
        }
    }

    private void updateWaypoints()
    {
        if (Vector3.Distance(rb.position, Waypoints[currentwaypoint]) < 500)
        {
            currentwaypoint = ++currentwaypoint % Waypoints.Length;
        }
    }

    private void flyTowardsTarget(Vector3 targetPos)
    {
        var delta = rb.position - targetPos;
        var yerror = delta.y;

        TARGETPITCH = Mathf.Clamp(-yerror, -MinPitch, MaxPitch);
        var pitch = Mathf.PI / 2 - Mathf.Acos(Vector3.Dot(transform.forward, Vector3.up));
        CURRENTPITCH = Mathf.Rad2Deg * pitch;

        var pitcherror = TARGETPITCH * Mathf.Deg2Rad - pitch;

        if (Vector3.Dot(transform.up, Vector3.up) < 0)
        {
            pitcherror *= -1;
        }

        pc.PitchAxis = VerticalSpeedPID.Update(pitcherror, Time.deltaTime);


        var angletotarget = Mathf.Atan2(delta.z, delta.x);
        var angle = Mathf.Atan2(transform.forward.z, transform.forward.x);

        TARGETANGLEDIFFERENCE = Mathf.DeltaAngle(angletotarget * Mathf.Rad2Deg + 180, angle * Mathf.Rad2Deg);

        TARGETANGLE = Mathf.Clamp(TARGETANGLEDIFFERENCE * AngleSensitivity, -MaxAngle, MaxAngle);

        if (TARGETPITCH > 5) TARGETANGLE *= 0.5f;

        Vector3 wantright = Vector3.ProjectOnPlane(transform.right, Vector3.up).normalized;

        if(Vector3.Dot(transform.up, Vector3.up) < 0)
        {
            wantright *= -1;
        }

        var error = -Vector3.Dot(transform.up, wantright) + Mathf.Sin(TARGETANGLE * Mathf.Deg2Rad);

        pc.RollAxis = RotationPID.Update(error, Time.deltaTime);
    }

    private void cruiseControl(float speed)
    {
        var mag = rb.velocity.magnitude;
        var error = Mathf.Clamp((mag - speed) * CruiseControlSensitivity, -10, 10);

        pc.Throttle = Mathf.Clamp01(ThrottlePID.Update(-error, Time.deltaTime));
    }

    private void matchDistance(Vector3 target, float distance)
    {
        var d = (target - transform.position).magnitude;
        var error = Mathf.Clamp((d - distance) * DistanceSensitivity, -100, 100);

        if(rb.velocity.magnitude < 35)
        {
            pc.Throttle = 1;
        }
        else
        {
            pc.Throttle = Mathf.Clamp01(ThrottlePID.Update(error, Time.deltaTime));
        }
        
    }

}