﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections.Generic;

public class MapManager : MonoBehaviour
{
    public Transform Player;

    public const float CellSize = 8000;

    public int OriginX;
    public int OriginY;

    public List<PlacedScene> Scenes;

    public void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(Vector3.zero, new Vector3(CellSize, 1, CellSize));
    }

    public void LoadLocation(int x, int y, bool forceUnload = false)
    {
        OriginX = x;
        OriginY = y;

        if (forceUnload)
        {
            unloadAllScenes(true);
            markAllUnloaded();
        }
        else
        {
            foreach (var r in FindObjectsOfType<SceneRoot>())
            {
                r.Reposition(OriginX, OriginY);
            }
        }

        refreshLoadedLevels();
    }

    public void Start()
    {

    }

    public void Update()
    {
        int dx = 0;
        int dy = 0;
        if (Player.position.x > CellSize / 2) dx++;
        if (Player.position.x < -CellSize / 2) dx--;
        if (Player.position.z > CellSize / 2) dy--;
        if (Player.position.z < -CellSize / 2) dy++;

        if (dx != 0 || dy != 0)
        {
            Player.position -= Vector3.right * CellSize * dx;
            Player.position += Vector3.forward * CellSize * dy;

            Vector3 off = Vector3.zero;
            off -= Vector3.right * CellSize * dx;
            off += Vector3.forward * CellSize * dy;

            foreach (Transform t in transform)
            {
                t.position += off;
            }

            OriginX += dx;
            OriginY += dy;

            refreshLoadedLevels();

            foreach (var r in FindObjectsOfType<SceneRoot>())
            {
                r.Reposition(OriginX, OriginY);
            }
        }
    }

    private void refreshLoadedLevels()
    {
        for (int i = 0; i < Scenes.Count; i++)
        {
            var ps = Scenes[i];

            if (ps.Loaded && shouldUnload(ps))
            {
                SceneManager.UnloadScene(ps.Index);
                ps.Loaded = false;
            }
            else if (!ps.Loaded && shouldLoad(ps))
            {
                SceneManager.LoadSceneAsync(ps.Index, LoadSceneMode.Additive);
                ps.Loaded = true;
            }

            Scenes[i] = ps;
        }
    }

    private bool shouldLoad(PlacedScene p)
    {
        int dx = OriginX - p.X;
        int dy = OriginY - p.Y;
        if (dx < 0) dx *= -1;
        if (dy < 0) dy *= -1;

        return (dx < 3 && dy < 3);
    }

    private bool shouldUnload(PlacedScene p)
    {
        int dx = OriginX - p.X;
        int dy = OriginY - p.Y;
        if (dx < 0) dx *= -1;
        if (dy < 0) dy *= -1;


        return (dx > 2 || dy > 2);
    }

    private void markAllUnloaded()
    {
        for (int i = 0; i < Scenes.Count; i++)
        {
            var p = Scenes[i];
            p.Loaded = false;
            Scenes[i] = p;
        }
    }

    private void unloadAllScenes(bool absoluteyallofthem = false)
    {
        Debug.Log("Unloading all scenes");

        for (int i = 0; i < Scenes.Count; i++)
        {
            var s = Scenes[i];
            if (s.Loaded || absoluteyallofthem)
            {
                s.Loaded = false;
                SceneManager.UnloadSceneAsync(s.Index);
            }
            Scenes[i] = s;
        }
    }
}

[Serializable]
public struct PlacedScene
{
    public int Index;
    public int X;
    public int Y;

    public bool Loaded;
}