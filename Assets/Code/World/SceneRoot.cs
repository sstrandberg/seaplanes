﻿using UnityEngine;

public class SceneRoot : MonoBehaviour
{
    public int CoordX;
    public int CoordY;

    public bool HasRef;

    public void OnValidate()
    {
        var mm = FindObjectOfType<MapManager>();
        HasRef = mm != null;
    }

    public void Reposition(int x, int y)
    {
        var dx = CoordX - x;
        var dy = y - CoordY;
        transform.position = new Vector3(dx * MapManager.CellSize, 0, dy * MapManager.CellSize);
    }

    public void Start()
    {
        var mm = FindObjectOfType<MapManager>();
        if(mm != null)
        {
            Reposition(mm.OriginX, mm.OriginY);
        }
    }
}