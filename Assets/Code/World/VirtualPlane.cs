﻿using System;
using UnityEngine;

[RequireComponent(typeof(PlaneManager))]
public class VirtualPlane : VirtualEntity
{
    public override void Virtualize()
    {
        IsVirtual = true;
        Position = transform.position;
        gameObject.SetActive(false);
    }

    public override void Devirtualize()
    {
        IsVirtual = false;
        transform.position = Position;
        gameObject.SetActive(true);
    }
}