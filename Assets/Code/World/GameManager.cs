﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    private PlayerData playerData;
    private WorldManager worldManager;
    private CameraFadeManager fadeManager;
    private MenuManager menuManager;

    public PlaneManager PlayerPlane;
    public Camera PlayerCamera;

    public GameState State;

    private Port currentPort;

    void Start()
    {
        playerData = GetComponent<PlayerData>();
        worldManager = GetComponent<WorldManager>();
        fadeManager = GetComponent<CameraFadeManager>();
        menuManager = GetComponent<MenuManager>();
    }


    void Update()
    {
        switch(State)
        {
            case GameState.Playing:
                if(Input.GetKeyDown(KeyCode.P))
                {
                    var port = getZonedPort();
                    if (port != null) enterPort(port);
                }
                break;
            case GameState.Port:
                break;

            case GameState.LoadingGameState:
                break;
        }
    }


    private Port getZonedPort()
    {
        var playerPos = PlayerPlane.transform.position;

        foreach(var p in FindObjectsOfType<Port>())
        {
            if(Mathx.BoundsCheck(p.LandingZoneMatrix, playerPos))
            {
                return p;
            }
        }
        return null;
    }

    private void enterPort(Port p)
    {
        currentPort = p;

        State = GameState.Port;

        fadeManager.FadeBetween(PlayerCamera, currentPort.Camera, 2);
        PlayerPlane.GetComponent<PlaneInput>().enabled = false;
        PlayerPlane.Throttle = 0;

        menuManager.LoadPort(p);
    }

    private void exitPort()
    {
        State = GameState.Playing;
    }
}

public enum GameState
{
    Playing,
    Port,
    LoadingGameState
}