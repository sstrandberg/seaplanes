﻿using UnityEngine;

public abstract class VirtualEntity : MonoBehaviour
{
    public Vector3 Position;
    public bool IsVirtual;

    public virtual void VirtualUpdate(float dt) { }

    public abstract void Virtualize();

    public abstract void Devirtualize();
}