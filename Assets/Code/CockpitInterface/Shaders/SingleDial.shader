﻿Shader "Dials/Single" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_DialTex ("Dial (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Rotation ("Rotation", Float) = 0.0
		_Offset ("Offset", Vector) = (0,0,0,0)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		inline float2 rotate(float2 pos, float angle){
				float sina = sin(angle);
				float cosa = cos(angle);
				return float2(pos.x * cosa - pos.y * sina, pos.y * cosa + pos.x * sina);
			}

		sampler2D _MainTex;
		sampler2D _DialTex;

		struct Input {
			float2 uv_MainTex;
			float2 uv_DialTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		half _Rotation;
		half4 _Offset;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_CBUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex.xy) * _Color;
			fixed4 dialColor = tex2D (_DialTex, rotate(IN.uv_DialTex - _Offset.xy, _Rotation) + _Offset.xy + _Offset.zw ) * _Color;
			o.Albedo = lerp(c.rgb, dialColor.rgb, dialColor.a);
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}

		

		ENDCG
	}
	FallBack "Diffuse"
}
