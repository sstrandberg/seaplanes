﻿using UnityEngine;

namespace CockpitInterface
{
    public class CockpitManager : MonoBehaviour
    {
        public LayerMask CockpitMask;

        private CockpitInteractable currentHovered;

        void Update()
        {
            var r = Camera.main.ScreenPointToRay(Input.mousePosition);

            CockpitInteractable newHovered = null;

            RaycastHit hit;
            if (Physics.Raycast(r, out hit, 1000, CockpitMask))
            {
                newHovered = hit.collider.gameObject.GetComponentInParent<CockpitInteractable>();
            }

            if(newHovered != currentHovered)
            {
                if (currentHovered) currentHovered.Hovered = false;
                if(newHovered) newHovered.Hovered = true;

                currentHovered = newHovered;
            }
        }

        public bool SomeState;

        public void DoSomeBoolThing(bool b)
        {
            Debug.Log(b);
        }
    }
}