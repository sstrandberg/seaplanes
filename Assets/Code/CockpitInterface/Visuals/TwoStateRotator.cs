﻿using UnityEngine;

namespace CockpitInterface.Visuals
{
    public class TwoStateRotator : MonoBehaviour
    {
        public Vector3 RotationFalse;
        public Vector3 RotationTrue;
        public float RotationTime = 0.1f;

        private float rotationTimer = 1;
        public bool CurrentState;

        public void UpdateState(object sender, bool state)
        {
            rotationTimer = 0;
            CurrentState = state;
        }

        void OnValidate()
        {
            transform.localRotation = Quaternion.Slerp(
                Quaternion.Euler(CurrentState ? RotationFalse : RotationTrue),
                Quaternion.Euler(!CurrentState ? RotationFalse : RotationTrue),
                rotationTimer);
        }

        void Update()
        {
            rotationTimer += Time.deltaTime / (Mathf.Max(RotationTime, 0.001f));

            transform.localRotation = Quaternion.Slerp(
                Quaternion.Euler(CurrentState ? RotationFalse : RotationTrue),
                Quaternion.Euler(!CurrentState ? RotationFalse : RotationTrue),
                rotationTimer);
        }
    }
}
