﻿using UnityEngine;

namespace CockpitInterface.Visuals
{
    public class Dial : MonoBehaviour
    {
        public PropEngine EngineRef;

        private Material material;

        public float Value;

        public float LowValue;
        public float HighValue;
        public float LowAngle;
        public float HighAngle;

        void Start()
        {
            material = GetComponent<MeshRenderer>().material;
            GetComponent<MeshRenderer>().sharedMaterial = material;
        }

        void Update()
        {
            //Value = EngineRef.RPM;
            var angle = Mathx.MapUnclamped(Value, LowValue, HighValue, LowAngle, HighAngle) % 360f;
            material.SetFloat("_Rotation", angle * Mathf.Deg2Rad);
        }
    }
}
