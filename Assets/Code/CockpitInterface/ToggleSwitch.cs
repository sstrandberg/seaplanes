﻿using UnityEngine;
using UnityEngine.Events;

namespace CockpitInterface
{
    public class ToggleSwitch : CockpitInteractable
    {
        public UnityEventBool OnToggle;
        public bool ToggleState;

        private bool clickDown;

        void Update()
        {
            clickDown |= Input.GetMouseButtonDown(0);
            clickDown &= Hovered;
            
            if(clickDown && Input.GetMouseButtonUp(0))
            {
                ToggleState = !ToggleState;
                OnToggle.Invoke(this, ToggleState);
            }            
        }
    }    
}
