﻿using System;
using UnityEngine;

public static class Mathx
{
   

    /// <summary>
    /// Normalizes a curve according to parameters.
    /// </summary>
    /// <param name="curve">The curve to normalize.</param>
    /// <param name="mintime">The minimum time on the modified curve.</param>
    /// <param name="maxtime">The max time on the modified curve.</param>
    /// <param name="minvalue">The minimum value on the modified curve.</param>
    /// <param name="maxvalue">The maximum value on the modified curve.</param>
    /// <param name="pointsonly">Should the curve be modified so that all parts are within the new range or only the control points?</param>
    /// <param name="curvesamples">The number of times to sample the curve to make sure all parts of the curve are within the new range. Only used if pointsonly is false.</param>
    public static void NormalizeCurve(AnimationCurve curve, float mintime = 0, float maxtime = 1, float minvalue = 0, float maxvalue = 1, bool pointsonly = false, int curvesamples = 100)
    {
        if (curve.keys.Length < 2) return;

        var k0 = curve.keys[0];
        var oldMinTime = k0.time;
        var oldMaxTime = k0.time;
        var oldMinValue = k0.value;
        var oldMaxValue = k0.value;

        for (int i = 1; i < curve.keys.Length; i++)
        {
            var k = curve.keys[i];
            oldMinTime = Mathf.Min(oldMinTime, k.time);
            oldMaxTime = Mathf.Max(oldMaxTime, k.time);
            oldMinValue = Mathf.Min(oldMinValue, k.value);
            oldMaxValue = Mathf.Max(oldMaxValue, k.value);
        }

        for (float t = 0; t < 1 && !pointsonly; t += 1f / curvesamples)
        {
            var e = curve.Evaluate(Mathf.Lerp(oldMinTime, oldMaxTime, t));
            oldMinValue = Mathf.Min(oldMinValue, e);
            oldMaxValue = Mathf.Max(oldMaxValue, e);
        }

        var newKeys = curve.keys;

        for (int i = 0; i < newKeys.Length; i++)
        {
            var k = newKeys[i];
            k.time = Map(k.time, oldMinTime, oldMaxTime, mintime, maxtime);
            k.value = Map(k.value, oldMinValue, oldMaxValue, minvalue, maxvalue);
            newKeys[i] = k;
        }

        curve.keys = newKeys;
    }

    public static float Map(float value, float inmin, float inmax, float outmin, float outmax)
    {
        return Mathf.Lerp(outmin, outmax, Mathf.InverseLerp(inmin, inmax, value));
    }

    public static float MapUnclamped(float value, float inmin, float inmax, float outmin, float outmax)
    {
        return (value - inmin) / (inmax - inmin) * (outmax - outmin) + outmin;
    }

    public const float MStoKnots = 1.94384f;
    public const float KnotsToMS = 0.514444f;

    public const float SaltWaterDensity = 1.025f;

    public static float TriangleArea(Vector3 a, Vector3 b, Vector3 c)
    {
        return Vector3.Cross((b - a), (c - a)).magnitude / 2;
    }

    public static float WrapAngle(Vector3 v)
    {
        return WrapAngle(v, Vector3.forward, Vector3.right);
    }

    public static float WrapAngle(Vector3 v, Vector3 forward, Vector3 right)
    {
        var r = Vector3.Angle(forward, v);
        if (Vector3.Dot(v, right) < 0)
        {
            r = 360 - r;
        }
        return r;
    }

    public static float DoubleLerp(float min, float zero, float max, float t)
    {
        if(t < 0)
        {
            return Mathf.Lerp(zero, min, -t);
        }
        else
        {
            return Mathf.Lerp(zero, max, t);
        }
    }

    public static float InterpolateLines(Func<float, float> fa, Func<float, float> fb, float t, float midpoint, float damping)
    {

        var halfdamp = damping / 2;

        if(t - midpoint < -halfdamp)
        {
            return fa(t);
        }
        else if(t - midpoint > halfdamp)
        {
            return fb(t);
        }
        else
        {
            var delta = midpoint - t;

            var a = fa(midpoint - halfdamp + (halfdamp - delta) * 0.5f);
            var b = fb(midpoint - (delta - halfdamp) * 0.5f);

            //return b;

            //return b;

            //var a = fa(midpoint - halfdamp - delta);
            //var b = fb(midpoint + delta * 0.5f);

            //return a;
            //return Mathf.Lerp(a, b, 0.5f);

            return Mathx.Map(delta, halfdamp, -halfdamp, a, b);
            //return (a + b) / 2;
        }
    }

    public static bool BoundsCheck(Vector3 Origin, Vector3 Size, Vector3 Point)
    {
        Size /= 2;
        return
            Point.x > Origin.x - Size.x &&
            Point.y > Origin.y - Size.y &&
            Point.z > Origin.z - Size.z &&
            Point.x < Origin.x + Size.x &&
            Point.y < Origin.y + Size.y &&
            Point.z < Origin.z + Size.z;
    }

    public static bool BoundsCheck(Matrix4x4 matrix, Vector3 point)
    {
        //var m = Matrix4x4.TRS(container.position, container.rotation, container.localScale);
        Vector3 tp = matrix.inverse.MultiplyPoint(point);
        return (tp.x > -.5f && tp.x < .5f && tp.y > -.5f && tp.y < .5f && tp.z > -.5f && tp.z < .5f);
    }
}

public static class Limit
{
    public static void OverEpsilon(ref float f)
    {
        if (f < Mathf.Epsilon) f = Mathf.Epsilon;
    }
}
    
public class VectorPid
{
    public float pFactor, iFactor, dFactor;

    private Vector3 integral;
    private Vector3 lastError;

    public VectorPid(float pFactor, float iFactor, float dFactor)
    {
        this.pFactor = pFactor;
        this.iFactor = iFactor;
        this.dFactor = dFactor;
    }

    public Vector3 Update(Vector3 currentError, float timeFrame)
    {
        integral += currentError * timeFrame;
        var deriv = (currentError - lastError) / timeFrame;
        lastError = currentError;
        return currentError * pFactor
            + integral * iFactor
            + deriv * dFactor;
    }
}

[System.Serializable]
public class FloatPID
{
    public float pFactor, iFactor, dFactor;

    private float integral;
    private float lastError;

    public FloatPID(float pFactor, float iFactor, float dFactor)
    {
        this.pFactor = pFactor;
        this.iFactor = iFactor;
        this.dFactor = dFactor;
    }

    public float Update(float currentError, float timeFrame)
    {
        integral += currentError * timeFrame;
        var deriv = (currentError - lastError) / timeFrame;
        lastError = currentError;
        return currentError * pFactor
            + integral * iFactor
            + deriv * dFactor;
    }
}

public static class Iterate
{
    public static void XY(ref int x, ref int y, int width)
    {
        x++;
        if (x >= width)
        {
            x = 0;
            y++;
        }
    }

    public static void XYZ(ref int x, ref int y, ref int z, int width, int height)
    {
        x++;
        if (x >= width)
        {
            x = 0;
            y++;
        }
        if (y >= height)
        {
            y = 0;
            z++;
        }
    }
}