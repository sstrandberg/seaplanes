﻿using UnityEngine.Events;

[System.Serializable]
public class UnityEventBool : UnityEvent<object, bool> { }