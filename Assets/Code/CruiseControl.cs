﻿using UnityEngine;

public class CruiseControl : MonoBehaviour
{
    public float TargetSpeed = 50;
    public float MaxForce = 1000;
    public float Buffer = 10;

    private Rigidbody rb;
    private PlaneManager pm;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        pm = GetComponent<PlaneManager>();
    }

    void FixedUpdate()
    {
        var mag = rb.velocity.magnitude;
        //var force = Mathx.Map(mag * Mathx.MStoKnots, TargetSpeed - Buffer, TargetSpeed, MaxForce, 0);
        //rb.AddRelativeForce(Vector3.forward * force);

        pm.Throttle = 1 - Mathf.InverseLerp(TargetSpeed - Buffer, TargetSpeed, mag * Mathx.MStoKnots);
    }
}