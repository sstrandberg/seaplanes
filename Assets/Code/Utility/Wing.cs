﻿using UnityEngine;

public class Wing : MonoBehaviour
{
    public PlaneManager Parent;

    public Foil Foil;


    [Header("Wing")]
    public bool Mirror;

    public int Sections = 1;
    public float InnerWidth = 1;
    public float OuterWidth = 1;
    public float Length = 2;
    public float Sweep = 0;
    public float Twist = 0;

    [Header("Control surface")]
    public bool HasControlSurface;
    public ControlType ControlSurfaceType = ControlType.Aileron;
    public int ControlSurfaceSectionStart;
    public int ControlSurfaceSectionEnd;
    public float ControlSurfaceProportion = 0.3f;
    private float controlSurfaceLine { get { return (InnerWidth + OuterWidth) / 2 * (-2 * ControlSurfaceProportion + 1); } }
    public float ControlSurfaceMinAngle = -30;
    public float ControlSurfaceMaxAngle = 30;

    public float ControlSurfaceAngle;

    [Space]
    [Header("graph")]

    public bool DrawGraphs;
    public float AngleRange = 90;
    public float VerticalRange = 1;

    private Vector3[] positions;
    private Vector3[] forces;
    private Color[] forcecolors;
    private Vector3[] velocities;

    private Rigidbody rb;

    public float LiftFactor = 1;

    void Awake()
    {
        rb = GetComponentInParent<Rigidbody>();
    }

    void Start()
    {
        Parent.AddWing(this);
    }

    void OnValidate()
    {
        if (Sections < 1) Sections = 1;
    }

    void OnDrawGizmos()
    {
        drawWing();

        if (DrawGraphs) drawGraph();
    }

    private void drawGraph()
    {
        Gizmos.color = Color.black;

        Gizmos.DrawLine(Vector3.left + Vector3.down, Vector3.right + Vector3.down);
        Gizmos.DrawLine(Vector3.right + Vector3.down, Vector3.right + Vector3.up);
        Gizmos.DrawLine(Vector3.right + Vector3.up, Vector3.left + Vector3.up);
        Gizmos.DrawLine(Vector3.left + Vector3.up, Vector3.left + Vector3.down);
        Gizmos.DrawLine(Vector3.left, Vector3.right);
        Gizmos.DrawLine(Vector3.up, Vector3.down);

        Gizmos.color = Color.gray;
        for (float f = 5; f < AngleRange; f += 5)
        {
            var off1 = Vector3.Lerp(Vector3.zero, Vector3.right, f / AngleRange);
            var off2 = -off1;

            Gizmos.DrawLine(Vector3.up + off1, Vector3.down + off1);
            Gizmos.DrawLine(Vector3.up + off2, Vector3.down + off2);
        }

        Gizmos.color = Color.blue;
        graph(Foil.GetLift, 200);
        Gizmos.color = Color.red;
        graph(Foil.GetDrag, 200);
    }

    private void drawWing()
    {
        var pos = transform.localPosition;
        //if (mirrored) pos.x *= -1;

        Color mainColor = Color.Lerp(Color.yellow, Color.red, 0.3f);
        Color controlSurfaceColor = Color.Lerp(Color.yellow, Color.green, 0.4f);

        for (int i = 0; i < Sections; i++)
        {
            var t0 = (float)(i) / Sections;
            var t1 = (float)(i + 1) / Sections;

            var wa = Mathf.Lerp(InnerWidth, OuterWidth, t0);
            var wb = Mathf.Lerp(InnerWidth, OuterWidth, t1);

            var wc = -wa;
            var wd = -wb;


            var x0 = Mathf.Lerp(0, Length, t0) * (Mirror ? -1 : 1);
            var x1 = Mathf.Lerp(0, Length, t1) * (Mirror ? -1 : 1);

            var sweepa = Mathf.Lerp(0, Sweep, t0);
            var sweepb = Mathf.Lerp(0, Sweep, t1);

            var sweepc = sweepa;
            var sweepd = sweepb;

            var q0 = Quaternion.AngleAxis(Mathf.Lerp(0, Twist, t0), Vector3.right);
            var q1 = Quaternion.AngleAxis(Mathf.Lerp(0, Twist, t1), Vector3.right);


            var drawcontrolsurface = HasControlSurface && i >= ControlSurfaceSectionStart && i < ControlSurfaceSectionEnd;
            if (drawcontrolsurface)
            {

                wc = -controlSurfaceLine - sweepc;
                wd = -controlSurfaceLine - sweepd;
            }

            drawQuad(mainColor,
                new Vector3(x0, 0, sweepa) + q0 * new Vector3(0, 0, wa), new Vector3(x1, 0, sweepb) + q1 * new Vector3(0, 0, wb),
                new Vector3(x0, 0, sweepc) + q0 * new Vector3(0, 0, wc), new Vector3(x1, 0, sweepd) + q1 * new Vector3(0, 0, wd));

            if (drawcontrolsurface)
            {
                var delta0 = -wa - wc;
                var delta1 = -wb - wd;

                var qq = Quaternion.AngleAxis(ControlSurfaceAngle, Vector3.right);

                drawQuad(controlSurfaceColor,
                    new Vector3(x0, 0, sweepc) + q0 * new Vector3(0, 0, wc), new Vector3(x1, 0, sweepd) + q1 * new Vector3(0, 0, wd),
                    new Vector3(x0, 0, sweepc) + q0 * new Vector3(0, 0, wc) + qq * q0 * new Vector3(0, 0, delta0), new Vector3(x1, 0, sweepd) + q1 * new Vector3(0, 0, wd) + qq * q1 * new Vector3(0, 0, delta1), true);
            }
        }
    }

    private void drawQuad(Color col, Vector3 a, Vector3 b, Vector3 c, Vector3 d, bool crossed = false)
    {
        Gizmos.color = col;
        Gizmos.DrawLine(transform.TransformPoint(a), transform.TransformPoint(b));
        Gizmos.DrawLine(transform.TransformPoint(b), transform.TransformPoint(d));
        Gizmos.DrawLine(transform.TransformPoint(d), transform.TransformPoint(c));
        Gizmos.DrawLine(transform.TransformPoint(c), transform.TransformPoint(a));

        if (crossed)
        {
            Gizmos.DrawLine(transform.TransformPoint(a), transform.TransformPoint(d));
            Gizmos.DrawLine(transform.TransformPoint(b), transform.TransformPoint(c));
        }
    }

    private void graph(System.Func<float, float> func, int segments)
    {
        float increment = AngleRange * 2 / segments;
        for (float f = -AngleRange; f < AngleRange; f += increment)
        {
            var ya = Mathx.Map(func(Mathf.Deg2Rad * f), -VerticalRange, VerticalRange, -1, 1);
            var yb = Mathx.Map(func(Mathf.Deg2Rad * (f + increment)), -VerticalRange, VerticalRange, -1, 1);

            var xa = Mathx.Map(f, -AngleRange, AngleRange, -1, 1);
            var xb = Mathx.Map(f + increment, -AngleRange, AngleRange, -1, 1);

            Gizmos.DrawLine(new Vector3(xa, ya), new Vector3(xb, yb));
        }
    }

    void FixedUpdate()
    {
        var dmg = GetComponent<Damageable>();
        LiftFactor = 1;
        if(dmg)
        {
            LiftFactor = dmg.Health / dmg.MaxHealth;
        }


        var csatarget = 0f;
        switch (ControlSurfaceType)
        {
            case ControlType.Aileron:
                csatarget = Mathx.DoubleLerp(ControlSurfaceMinAngle, 0, ControlSurfaceMaxAngle, Mirror ? -Parent.RollControl : Parent.RollControl);
                break;
            case ControlType.Elevator:
                csatarget = Mathx.DoubleLerp(ControlSurfaceMinAngle, 0, ControlSurfaceMaxAngle, Parent.PitchControl);
                break;
            case ControlType.Rudder:
                csatarget = Mathx.DoubleLerp(ControlSurfaceMinAngle, 0, ControlSurfaceMaxAngle, -Parent.YawControl);
                break;
        }

        ControlSurfaceAngle = Mathf.MoveTowards(ControlSurfaceAngle, csatarget, 60 * Time.fixedDeltaTime);
        ControlSurfaceAngle = csatarget;
    }

    public void CalcForces()
    {
        var airdensity = Environment.Instance.AirDensity(rb.position.y);
        
        int count = 0;
        for (int i = 0; i < Sections; i++)
        {
            count++;
            if (HasControlSurface && i >= ControlSurfaceSectionStart && i < ControlSurfaceSectionEnd)
            {
                count++;
            }
        }

        if (positions == null || positions.Length != count)
        {
            positions = new Vector3[count];
            forces = new Vector3[count];
            velocities = new Vector3[count];
            forcecolors = new Color[count];
        }

        int index = 0;
        for (int i = 0; i < Sections; i++)
        {
            var t = (i + 0.5f) / Sections;

            var x = Mathf.Lerp(0, Length, t) * (Mirror ? -1 : 1);
            var width = Length / Sections;
            var high = Mathf.Lerp(InnerWidth, OuterWidth, t);
            var low = -high;
            var controllow = low;
            var sweep = Mathf.Lerp(0, Sweep, t);
            var twist = Mathf.Lerp(0, Twist, t);

            var iscontrolsurface = HasControlSurface && i >= ControlSurfaceSectionStart && i < ControlSurfaceSectionEnd;

            if (iscontrolsurface)
            {
                low = -controlSurfaceLine - sweep;
            }

            var rot = Quaternion.AngleAxis(twist, Vector3.right);
            var pos = transform.TransformPoint(new Vector3(x, 0, sweep) + rot * new Vector3(0, 0, 0.5f * (high + low)));
            var vel = -rb.GetPointVelocity(pos) + Environment.Instance.WindVelocity;

            var force = getForce(vel, twist * Mathf.Deg2Rad, (high - low) * width, airdensity, out forcecolors[index]);
           
            positions[index] = pos;
            forces[index] = force;
            velocities[index] = vel;
            index++;

            if (iscontrolsurface)
            {
                var csrot = Quaternion.AngleAxis(ControlSurfaceAngle, Vector3.right);
                var cspos = transform.TransformPoint(new Vector3(x, 0, sweep) + rot * new Vector3(0, 0, low) + csrot * rot * new Vector3(0, 0, (controllow - low) / 2));
                var csvel = -rb.GetPointVelocity(cspos) + Environment.Instance.WindVelocity;
                //var csvel = -Parent.RB.velocity;

                var csforce = getForce(csvel, (twist + ControlSurfaceAngle) * Mathf.Deg2Rad, (low - controllow) * width, airdensity, out forcecolors[index]);

                positions[index] = cspos;
                forces[index] = csforce;
                velocities[index] = csvel;
                index++;
            }
        }
    }

    public void AddForces(int substeps)
    {
        var debugscaling = 0.05f;


        for (int i = 0; i < positions.Length; i++)
        {
            //var maga = smoothedforces[i].magnitude;
            //var magb = forces[i].magnitude;
            //var dir = magb == 0f ? Vector3.forward : forces[i] / magb;
            //smoothedforces[i] = Mathf.Lerp(maga, magb, 1 - ForceSmoothing) * dir;
            rb.AddForceAtPosition(forces[i] / substeps, positions[i]);
            //Debug.DrawRay(positions[i], forces[i] * debugscaling, forcecolors[i]);
        }
    }

    private Vector3 getForce(Vector3 velocity, float tilt, float surfacearea, float airdensity, out Color c)
    {
        c = Color.blue;

        var locvel = transform.InverseTransformVector(-velocity);

        var projvel = Vector3.ProjectOnPlane(velocity, transform.right);

        projvel = Vector3.ClampMagnitude(projvel, 200);
        c = projvel.magnitude > 199 ? Color.red : Color.blue;

        var aoa = Mathf.Atan2(locvel.y, locvel.z);

        var cL = Foil.GetLift(aoa + tilt);
        var cD = Foil.GetDrag(aoa + tilt);

        var liftdir = Vector3.Cross(projvel, transform.right).normalized;
        var dragdir = projvel.normalized;

        cL *= LiftFactor;

        return (liftdir * cL + dragdir * cD) * 0.5f * surfacearea * airdensity * projvel.sqrMagnitude;
    }

   
}

public enum ControlType
{
    Aileron, Elevator, Rudder
}