﻿using UnityEngine;
using System.Collections.Generic;

public class PlaneController : MonoBehaviour
{
    public Rigidbody RB
    {
        get
        {
            if (rb == null) rb = GetComponent<Rigidbody>();
            return rb;
        }
    }
    private Rigidbody rb;


    [Range(-1f, 1f)]
    public float YawAxis;
    public float YawTrim;
    public float YawControl
    {
        get
        {
            return Mathf.Clamp(YawAxis + YawTrim, -1, 1);
        }
    }

    [Range(-1f, 1f)]
    public float PitchAxis;
    public float PitchTrim;
    public float PitchControl
    {
        get
        {
            return Mathf.Clamp(PitchAxis + PitchTrim, -1, 1);
        }
    }

    [Range(-1f, 1f)]
    public float RollAxis;
    public float RollTrim;
    public float RollControl
    {
        get
        {
            return Mathf.Clamp(RollAxis + RollTrim, -1, 1);
        }
    }

    public float Status;

    [Range(0f, 1f)]
    public float Throttle;

    private Wing[] wings;

    public bool Shooting;

    void Update()
    {
        var resetspeed = 0f;
        YawAxis = Mathf.MoveTowards(YawAxis, 0, resetspeed * Time.deltaTime);
        PitchAxis = Mathf.MoveTowards(PitchAxis, 0, resetspeed * Time.deltaTime);
        RollAxis = Mathf.MoveTowards(RollAxis, 0, resetspeed * Time.deltaTime);
    }

    void Awake()
    {
        wings = new Wing[0];
        //wings = GetComponentsInChildren<Wing>();
    }


    public void AddWing(Wing w)
    {
        var l = new List<Wing>(wings);
        l.Add(w);
        wings = l.ToArray();
    }

    public int SubSteps = 10;


    void FixedUpdate()
    {
        int substeps = SubSteps;

        var oldvelocity = RB.velocity;
        for (int i = 0; i < substeps; i++)
        {
            for (int j = 0; j < wings.Length; j++)
            {
                wings[j].CalcForces();
            }
            for (int j = 0; j < wings.Length; j++)
            {
                wings[j].AddForces(substeps);
            }
        }
       
        //rb.velocity = Vector3.ClampMagnitude(rb.velocity, oldvelocity.magnitude);
        //var delta = rb.velocity - oldvelocity;
        //delta = Vector3.ClampMagnitude(delta, 50);
        //rb.velocity = oldvelocity + delta;

        Status = rb.velocity.magnitude;
    }
}