﻿using UnityEngine;

public class Gun : MonoBehaviour
{
    public float RateOfFire;
    public float MuzzleVelocity = 300;
    public Bullet Bullet;

    public Vector3 Offset;

    private float cool;
    private PlaneManager pm;

    private bool mirror;

    void Start()
    {
        pm = GetComponent<PlaneManager>();
    }

    void FixedUpdate()
    {
        cool += Time.fixedDeltaTime * RateOfFire;

        while(cool > 1 && pm.Shooting)
        {
            cool--;
            mirror = !mirror;
            shoot();
        }
        cool = Mathf.Clamp01(cool);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        var off = Offset;
        Gizmos.DrawRay(transform.TransformPoint(off), transform.forward);
        off.x *= -1;
        Gizmos.DrawRay(transform.TransformPoint(off), transform.forward);
    }

    private void shoot()
    {
        var off = Offset;
        if (mirror) off.x *= -1;
        var b = Instantiate(Bullet, transform.TransformPoint(off), transform.rotation, null);
        b.GetComponent<Bullet>().Velocity = transform.forward * MuzzleVelocity;
    }
}