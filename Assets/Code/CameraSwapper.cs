﻿using UnityEngine;

public class CameraSwapper : MonoBehaviour
{
    public Transform CameraTarget;

    public float MainFOV = 90;
    public float ZoomFOV = 60;

    public bool LookAhead = true;
    public bool Zoom = false;

    private Camera c;

    void Start()
    {
        c = GetComponent<Camera>();
    }

    void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            LookAhead = !LookAhead;
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            Zoom = !Zoom;
        }

        var q = Quaternion.identity;
        var wantfov = Zoom ? ZoomFOV : MainFOV;
        if (!LookAhead)
        {
            q = Quaternion.LookRotation(CameraTarget.position - transform.position);
        }
        else
        {
            q = Quaternion.LookRotation(transform.parent.forward);
            wantfov = MainFOV;
        }

        transform.rotation = Quaternion.Slerp(transform.rotation, q, 0.2f);
        c.fieldOfView = Mathf.Lerp(c.fieldOfView, wantfov, 0.2f);
    }
}