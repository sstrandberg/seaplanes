﻿using UnityEngine;

public class Damageable : MonoBehaviour
{
    public bool Penetratable;
    public float Armor;
    public float Health = 10;
    public float MaxHealth = 10;
    public float MaxDamage;

    public void Damage(float damage, float apfactor, float cosTheta)
    {

        var effectiveArmor = Armor / Mathf.Max(cosTheta, Mathf.Epsilon);

        //figure out the amount of damage done
        var potentialDamage = Mathf.Max(damage - effectiveArmor / Mathf.Exp(apfactor), 0);
        if (MaxDamage > 0)
        {
            potentialDamage = Mathf.Max(potentialDamage, MaxDamage);
        }
        var actualDamage = Mathf.Min(potentialDamage, Health);

        Debug.Log("Hit for " + actualDamage + " damage");

        Health -= actualDamage;
    }
}