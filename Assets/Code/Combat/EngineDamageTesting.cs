﻿using UnityEngine;

public class EngineDamageTesting : MonoBehaviour
{
    private PropEngine engine;
    private Damageable damageable;
    private ParticleSystem particlesystem;

    public bool EngineAlive = true;

    void Start()
    {
        engine = GetComponent<PropEngine>();
        damageable = GetComponent<Damageable>();
        particlesystem = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        if(EngineAlive && (damageable.Health == 0 || transform.position.y < 0))
        {
            EngineAlive = false;
            //engine.enabled = false;
            var em = particlesystem.emission;
            em.enabled = true;
        }

        if (!EngineAlive)
        {
            engine.BasePower = 0;
        }
    }
}