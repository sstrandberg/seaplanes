﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float Damage;
    public float APFactor = 0;

    public float Mass = 0.04f;

    public float Drag;

    [HideInInspector]
    public Vector3 Velocity;
    [HideInInspector]
    public Vector3 Position;
    private Vector3 lastPosition;

    public LayerMask CollisionMask;



    public void AddForce(Vector3 force, ForceMode mode)
    {
        if (Mass < 0.001f) Mass = 0.001f;

        var f = force;
        if (mode == ForceMode.Acceleration || mode == ForceMode.Force)
        {
            f *= Time.fixedDeltaTime;
        }
        if (mode == ForceMode.Force || mode == ForceMode.Impulse)
        {
            f /= Mass;
        }

        Velocity += f;
    }

    void Start()
    {
        Destroy(gameObject, 5);
    }

    void OnValidate()
    {
        Limit.OverEpsilon(ref Mass);
    }

    void FixedUpdate()
    {
        if(Position != transform.position)
        {
            Position = transform.position;
            lastPosition = Position;
        }
       
        AddForce(Physics.gravity, ForceMode.Acceleration);
        Velocity = Vector3.MoveTowards(Velocity, Vector3.zero, 
            Time.fixedDeltaTime * Velocity.sqrMagnitude * Drag / Mass);

        var nextPostion = Position + Velocity * Time.fixedDeltaTime;

        var fcm = FastColliderManager.Instance;

        foreach (var c in fcm.Colliders)
        {
            traceCollisions(Position + c.Delta, nextPostion, c);
        }
        traceCollisions(Position, nextPostion, null);

        Debug.DrawLine(Position, nextPostion, Color.blue);

        lastPosition = Position;
        Position = nextPostion;
        transform.position = Position;
    }

    private bool traceCollisions(Vector3 from, Vector3 to, FastCollider targetCollider)
    {
        var pos = from;
        int count = 0;
        while(true)
        {

            if (count++ > 10) return false;

            var r = new Ray(pos, to - pos);
            RaycastHit hit;
            if(Physics.Raycast(r, out hit, (to - pos).magnitude, CollisionMask))
            {
                var hitCollider = hit.collider.GetComponentInParent<FastCollider>();
                //null == null also
                if(targetCollider == hitCollider)
                {
                    var dmg = hit.collider.GetComponentInParent<Damageable>();

                    if (dmg)
                    {

                        

                        dmg.Damage(Damage, APFactor, Vector3.Dot(Velocity.normalized, -hit.normal));

                        var lingertime = 1.5f;
                        Debug.DrawRay(hit.point, Vector3.up, Color.red, lingertime);
                        Debug.DrawRay(hit.point, Vector3.down, Color.red, lingertime);
                        Debug.DrawRay(hit.point, Vector3.left, Color.red, lingertime);
                        Debug.DrawRay(hit.point, Vector3.right, Color.red, lingertime);
                        Debug.DrawRay(hit.point, Vector3.forward, Color.red, lingertime);
                        Debug.DrawRay(hit.point, Vector3.back, Color.red, lingertime);

                        if (!dmg.Penetratable)
                        {
                            Destroy(gameObject);
                            break;
                        }
                        else
                        {
                            pos = hit.point + Velocity * 0.00001f;
                            continue;
                        }

                    }
                    else
                    {
                        //fatal collision with terrain or whatever
                        break;
                    }
                }
                else
                {
                    pos = hit.point;
                    continue;
                }
            }

            break;
        }

        return false;
        
    }

    private float timer;

    void Update()
    {
        timer += Time.deltaTime;
        if(timer > 30)
        {
            Destroy(gameObject);
        }
    }
}



//register both passthrough and solid collisions