﻿using UnityEngine;

public class CameraFadeTester : MonoBehaviour
{

    public Camera CameraA;
    public Camera CameraB;
    public float FadeTime = 1;

    public bool DoTest;

    private bool flipped;

    void Update()
    {
        if(DoTest)
        {
            DoTest = false;
            flipped = !flipped;
            GetComponent<CameraFadeManager>().FadeBetween(
                flipped ? CameraA : CameraB,
                flipped ? CameraB : CameraA, FadeTime);
        }
    }
}