﻿using System;
using UnityEngine;

public class CameraFadeManager : MonoBehaviour
{
    public Shader CameraFadeShader;

    public static CameraFadeManager Instance
    {
        get
        {
            if (instance == null) instance = FindObjectOfType<CameraFadeManager>();
            return instance;
        }

    }
    private static CameraFadeManager instance;

    public bool fading;

    public Camera camA;
    public Camera camB;
    public float fadeFactor;
    public float fadeRate;
    public RenderTexture rt;
    public CameraFadeEffect effect;


    public void FadeBetween(Camera fadeFrom, Camera fadeTo, float time)
    {
        if (fading) endFade();

        if (fadeFrom == null) throw new ArgumentNullException("fadeFrom");
        if (fadeTo == null) throw new ArgumentNullException("fadeTo");
        if (Single.IsNaN(time)) throw new ArgumentOutOfRangeException("time");

        camA = fadeFrom;
        camB = fadeTo;

        fadeFactor = 0;
        time = Mathf.Max(time, Mathf.Epsilon);
        fadeRate = 1 / time;

        startFade();
    }

    void Update()
    {
        if (fading)
        {
            fadeFactor += Time.deltaTime * fadeRate;
            if(fadeFactor > 1)
            {
                endFade();
            }
            else
            {
                //effect.FadeFactor = fadeFactor;
                effect.FadeFactor = Easing.QuadInOut(fadeFactor);
            }
        }
    }

    void startFade()
    {
        fading = true;

        rt = RenderTexture.GetTemporary(Screen.width, Screen.height, 24, RenderTextureFormat.Default, RenderTextureReadWrite.Default, 4);

        camA.enabled = true;
        camA.targetTexture = rt;
        camB.enabled = true;
        effect = camB.gameObject.AddComponent<CameraFadeEffect>();
        effect.CameraFadeShader = CameraFadeShader;
        effect.FadeFrom = rt;
        effect.FadeFactor = 0;
    }

    void endFade()
    {
        fading = false;

        camA.enabled = false;
        camA.targetTexture = null;
        camB.enabled = true;
        Destroy(effect);

        RenderTexture.ReleaseTemporary(rt);
    }
}