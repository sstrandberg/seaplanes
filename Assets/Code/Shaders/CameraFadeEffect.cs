﻿using UnityEngine;

[ExecuteInEditMode]
public class CameraFadeEffect : MonoBehaviour
{
    public Shader CameraFadeShader;
    private Material material;

    public RenderTexture FadeFrom;
    [Range(0, 1)]
    public float FadeFactor;

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (material == null)
        {
            if (CameraFadeShader == null) return;
            material = new Material(CameraFadeShader);
            material.hideFlags = HideFlags.HideAndDontSave;
        }

        material.SetFloat("_FadeFactor", FadeFactor);
        Graphics.Blit(source, destination, material, 0);
        Graphics.Blit(FadeFrom, destination, material, 1);
    }
}