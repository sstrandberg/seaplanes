﻿using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Foil")]
public class Foil : ScriptableObject
{
    [Header("Foil")]
    public float LiftPower = 1;
    public float MinDrag = 0.05f;
    public float DragPower = 1;
    public float Camber = 5;
    public float StallPoint = 30;
    [Range(0, 20)]
    public float StallSmoothing = 7;

    public float NoLiftPoint = 40;
    [Range(0, 20)]
    public float NoliftSmoothing = 14;

    public float GetLift(float alpha)
    {
        var adjustedalpha = alpha + Mathf.Deg2Rad * Camber;
        var absalpha = Mathf.Abs(adjustedalpha);

        Func<float, float> prestall = (t => 2 * Mathf.PI * t);

        var stally = prestall(StallPoint * Mathf.Deg2Rad);
        var stalline = -stally / ((NoLiftPoint - StallPoint) * Mathf.Deg2Rad);

        Func<float, float> stall = (t => stally + (t - StallPoint * Mathf.Deg2Rad) * stalline);
        Func<float, float> nolift = (t => 0);


        var lift = prestall(absalpha);


        float value;

        if (absalpha < Mathf.Deg2Rad * (StallPoint + NoLiftPoint) / 2)
        {
            value = Mathx.InterpolateLines(prestall, stall, absalpha, StallPoint * Mathf.Deg2Rad, StallSmoothing * Mathf.Deg2Rad);
        }
        else
        {
            value = Mathx.InterpolateLines(stall, nolift, absalpha, NoLiftPoint * Mathf.Deg2Rad, NoliftSmoothing * Mathf.Deg2Rad);
        }


        if (adjustedalpha < 0) value *= -1;


        return value * LiftPower;

    }

    public float GetDrag(float alpha)
    {
        return (Mathf.Abs(1 - Mathf.Cos(2 * alpha))) * DragPower + MinDrag;
        //return Mathf.Pow(10 * alpha, 2);
    }   
}