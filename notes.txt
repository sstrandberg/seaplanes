AIRPLANE FORMAT
	need to deserialize:
		load gameobject tree
		set up proper settings (different objects?)
		FUSELAGE
			
		WINGS
		ENGINES
			slot type?
			referenced separately
		GUNS	
			slot type
			referenced separately

INTERFACE
	no idea for most of this but
	weight-based ammo counter
	altimeter with lag	

physics
	engine
	airbrakes
	spoilers
	flaps/slats
	body lift
	proper guns
Combat
	Engine damage
		piston exploding
		gasket cap?
		damage to main engine -> less horsepower
		overheating -> damage to engine, cooler, less horsepower
AI
	add back throttle control
	takeoff/landing


game loop things
	open world
	planes flying from place to place
		takeoff - navigate - land
	ports
		fade to white -> interface
		menus for whatever
			buying/selling
			swapping planes
			upgrading planes
			taking on missions
			have a cute background?
		fade to white -> game
	ships
	planes as entities separate from physics
		upgrades too
	inventory?
	missions
		bounty hunting
		protect something
		deliver something



AIRPLANE:
	main
	controller (AI/player/nothing)
	components
		fuselage/floats
		fuel tanks
		engine
		props
		wings
		guns
		magazines?
		storage



CODE
	Floating origin
	Spawning planes
		done from parts
		SO?
	Docking
	Spawning AI planes
	Missions
	Interface stuff

	airplane things:
		proper split up for controller vs central thing
		format for airplanes
			incl. upgrades




ART
	animated planes
	shader things (cel + lines)
	water (how?)
	terrain
	towns

AUDIO
	wind
	engine
	guns

UPGRADES
	fuselage
		drag
		mass
		fuel tanks
		storage
		magazines
		gun size
		engine size
	wings
		mass
		drag
		control surface responsiveness
		surface area
	engines
		damage resistance
		fuel efficiency
		power
		max RPM
		air intake
			super/turbo chargers?
		heating
		cooling
		thrust vectoring
	guns
		RPM
		ammo type
		velocity
		magazine size
		jam rate


ACTUAL UPGRADE SLOTS
	FUSELAGE
		weight reduction
		increasing fuel tank size
		stiffness
		engine mount
		gun mounts / magazine size
		flight control system
	WINGS
		weight reduction
		shape
		control surfaces
	ENGINES
		intake
		power
		cooling systems
		propellers
	GUNS
		barrel
		loader
		cooling
		receiver






all the different parts with locations (incl alt-part location?)
	use a prefab?

	parent thing has:
		planecontroller thing
		fuselage upgrades
		

		fuselage
			gun mounts
			prop mounts
			engine mounts

			wing shapes
			wing reinforcement
			flight control system reinforcement

			interior layout options

		prop mounts:
			swappable props (size)

		engine mounts:
			swappable engines (size + reinforcement level)

		gun mounts:
			swappable guns (size + reinforcement level + magazine size)

		engines:
			

		guns:





PLANE
	- control logic
		- hooks to everything
	- controller
	
FUSELAGE (in several parts)
	- bouyancy
	- aero physics?
	- colliders
	- attachment to other fuselage parts
	- vis

FUEL TANKS
	- colliders
	- attachment to fuselage (fixed)
	- fuel tank logic

WING
	- wing logic
	- bouyancy
	- colliders
	- vis
	- attachment to fuselage/wing

ENGINE
	- engine logic
	- colliders
	- vis
	- attachement to fuselage/wing
	- bouyancy

PROP
	- prop logic
	- collider?
	- attachment to engine
	- vis

GUN
	- gun logic
	- colliders
	- vis

MAGAZINE
	- magazine logic
	- collider
	- attachment to fuselage

STORAGE
	- storage logic
	- collider
	- attachmenet to fuselage




so what do I actually want to implement
	guns
	AI that can fire guns
	something that spawns complete airplanes (world manager?)



WHAT IS GAME
	active/inactive sections?
		done via scene loading?
		could just port over the one from witchgame
	active/inactive entities


	world view:
		updating active entities
		updating inactive entities
		loading/unloading terrain

	town:
		update on leaving



PLAYER MOVING
PLAYER LANDING AT PORTS
CHANGING SCENES
UPGRADE OPTIONS
RESTARTING GAME SCENE

PORT -> trigger with link to port data

what's in a port
	name
	available upgrades?
		mechanic level in certain things
			common -> rare -> unique?
	inventory for buying/selling


store
bar







things that need to work:
	virtual airplanes
		make them exist
	spawning airplanes from virtual airplanes
		dummy spawner script that just returns prefab for now?
	assigning AI to spawned airplanes and giving goals to them	